const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const passport = require('passport')

const userRoutes = require('./routes/user');
const propertyRoutes = require('./routes/property');
const PORT = 4000;

app.use(cors());
app.use('/src',express.static('src'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// connect mongoDB
const db = require('./config/keys').mongodbURI;
mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology:true});

const connect = mongoose.connection;

connect.once('open',function () {
    console.log('Database connected success.'); 
});

// passport middleware
app.use(passport.initialize());

// passport config
require('./config/passport') (passport);

// call routes here
app.use('/apis/',userRoutes);
app.use('/apis/',propertyRoutes);
app.listen(PORT,function(){
    console.log("Havitat backend running at port " + PORT);
});