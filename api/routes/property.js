const express = require('express');
const HavitatRoutes = express.Router();
const keys = require('../config/keys');

const User = require('../model/User');
const Property = require('../model/Property');

// add multer for upload file
const multer = require('multer');

// upload file consts

const storage = multer.diskStorage({
    destination: function(req,res,cb){
        cb(null,'src/')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

const fileFilter = (req,file,cb) =>{
    if(file.mimetype === "image/jpeg" || file.mimetype === "image/png"){
        cb(null, true)
    }else{
        cb(null,false)
    }
}

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

HavitatRoutes.post('/create-property', upload.array('property_images',6) , (req,res,next) => {
    const reqFiles = [];

    for(var i = 0; i < req.files.length; i++){
        reqFiles.push('src/' + req.files[i].filename)
    }

    let obj = {
        user_id: req.body.user_id,
        property_title: req.body.property_title,
        property_address: req.body.property_address,
        property_transaction_category: req.body.property_transaction_category,
        property_type: req.body.property_type,
        property_sub_type: req.body.property_sub_type,
        property_images: reqFiles,
        property_details: JSON.parse(req.body.property_details),
        location_info: JSON.parse(req.body.location_info),
        neighborhood_info: JSON.parse(req.body.neighborhood_info),
        offer_price: JSON.parse(req.body.offer_price)
    }

    Property.create(obj, (err,item)=>{
        if(err)
            throw err;
        else
            res.status(200).json({ "status":"success", "message":"Property added success" });
    })
});

HavitatRoutes.post('/update-property', upload.array('property_images',6), (req,res,next) => {
    const reqFiles = [];

    for(var i = 0; i < req.files.length; i++){
        reqFiles.push('src/' + req.files[i].filename );
    }

    let update_obj = {
        user_id: req.body.user_id,
        property_title: req.body.property_title,
        property_address: req.body.property_address,
        property_transaction_category: req.body.property_transaction_category,
        property_type: req.body.property_type,
        property_sub_type: req.body.property_sub_type,
        property_images: reqFiles,
        property_details: JSON.parse(req.body.property_details),
        location_info: JSON.parse(req.body.location_info),
        neighborhood_info: JSON.parse(req.body.neighborhood_info),
        offer_price: JSON.parse(req.body.offer_price)
    }

    Property.findById(req.body.property_id, function(err, property){
        if(err){
            res.status(400).json({"status":"error","data":err});
        }else{
            property.user_id = req.body.user_id;
            property.property_title = req.body.property_title;
            property.property_address = req.body.property_address;
            property.property_transaction_category = req.body.property_transaction_category;
            property.property_type = req.body.property_type;
            property.property_sub_type = req.body.property_sub_type;
            property.property_images = reqFiles;
            property.property_details = JSON.parse(req.body.property_details);
            property.location_info = JSON.parse(req.body.location_info);
            property.neighborhood_info = JSON.parse(req.body.neighborhood_info);
            property.offer_price = JSON.parse(req.body.offer_price);
            property.save().then(property => {
                res.status(200).json({"status":"success","message":"Your Property updated successfully."});
            }).catch(err => { console.log(err); });
        }
    });
});

HavitatRoutes.get('/get-all-properties', (req,res) => {
    Property.find((err,property)=>{
        if(err)
            throw err;
        else
            res.status(200).json({ "status":"success", "data": property });
    });
});

HavitatRoutes.get('/get-user-properties/:user_id', (req,res) => {

    Property.find({ user_id: req.params.user_id, property_transaction_category: req.query.types }).then(property => {
        res.status(200).json({"status":"success", "data":property});
    })
    .catch(err => { res.status(400).json(err)} );
});

HavitatRoutes.get('/get-sale-property',(req,res) => {

    let filter = '';
    if(req.query.filter === 'LtoH'){
        filter = {"offer_price.offer_price":-1};
    }else{
        filter = {"offer_price.offer_price":1};
    }
    
    Property.find(
    {property_transaction_category: "For Sale",
    property_sub_type: req.query.sub_type,
    $or:[
        {property_title: { $regex: '.*'+ req.query.search +'.*' } }
    ]
    }).sort(filter).then(property => {
        res.status(200).json({"status":"success","data":property});
    }).catch(err => { res.status(400).json(err) });
});

HavitatRoutes.get('/get-rent-properties', (req,res) => {
    let filter = '';
    if(req.query.filter === 'LtoH'){
        filter = {"offer_price.offer_price":-1};
    }else{
        filter = {"offer_price.offer_price":1};
    }
    
    Property.find(
    {property_transaction_category: "For Rent",
    property_sub_type: req.query.sub_type,
    $or:[
        {property_title: { $regex: '.*'+ req.query.search +'.*' } }
    ]
    }).sort(filter).then(property => {
        res.status(200).json({"status":"success","data":property});
    }).catch(err => { res.status(400).json(err) });
});

HavitatRoutes.get('/get-joint-venture-properties', (req,res) =>{
    Property.find({ property_transaction_category:'For Joint Venture' }).then(property =>{
        res.status(200).json({"status":"success", "data":property});
    }).catch(err => { res.status(400).json(err) });
});

HavitatRoutes.get('/get-property-details/:property_id',(req,res) => {
    Property.findOne({_id: req.params.property_id}).then(property => {
        res.status(200).json({"status":"success","data":property});
    }).catch(err => { res.status(400).json(err) });
});


HavitatRoutes.post('/delete-rent-property/:property_id',(req,res) => {
    Property.deleteOne({_id: req.params.property_id}).then(property => {
        res.status(200).json({"status":"success","message":"Your property removed successfully."});
    }).catch(err => { res.status(400).json(err) });
});


HavitatRoutes.post('/delete-sale-property/:property_id',(req,res) => {
    Property.deleteOne({_id: req.params.property_id}).then(property => {
        res.status(200).json({"status":"success","message":"Your property removed successfully."});
    }).catch(err => { res.status(400).json(err) });
});


module.exports = HavitatRoutes;