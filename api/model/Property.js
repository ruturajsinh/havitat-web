const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PropertySchema = new Schema({
    user_id:{
        type: String
    },
    property_title:{
        type: String
    },
    property_address:{
        type: String
    },
    property_transaction_category:{
        type: String
    },
    property_type:{
        type: String
    },
    property_sub_type:{
        type: String
    },
    property_images:{
        type: Array
    },
    property_details:{
        type: Object
    },
    location_info:{
        type: Object
    },
    neighborhood_info:{
        type: Object
    },
    offer_price:{
        type: Object
    }

});

module.exports = mongoose.model('properties',PropertySchema);