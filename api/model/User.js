const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name:{
        type: String
    },
    email:{
        type: String
    },
    password:{
        type: String
    },
    number:{
        type: Number
    },
    token:{
        type: String
    },
    google_id:{
        type: String
    },
    facebook_id:{
        type: String
    },
    signup_by:{
        type: String
    },
    property:{
        type: Array
    }
});

module.exports = mongoose.model("users",UserSchema);