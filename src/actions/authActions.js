import axios from 'axios'
import setAuthToken from '../utils/setAuthToken'
import jwt_decode from 'jwt-decode'
import Cookie, { set } from 'js-cookie';
import { GET_ERRORS,USER_LOADING,SET_CURRENT_USER } from './types'
import firebase from 'firebase'
import firebaseConfig from '../firebaseConfig'

// register user
export const registerUser = (userData, history) => dispatch => {
    axios
        .post("/register", userData)
        .then( res => window.location.href = '/login')
        .catch(err => dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
};

// register method when user signup wit mobile number otp

export const registerWithOtp = userData => dispatch => {
    axios.post('/register-with-otp', userData).then(res => {
        const { token } = res.data;

        localStorage.setItem("jwtToken", token);

        const decoded = jwt_decode(token);
        dispatch(setCurrentUser(decoded));
    })
    .catch(err => 
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    );
}

// login user and set auth token
export const loginUser = userData => dispatch => {
    axios
        .post('/login', userData)
        .then(res => {
            const { token } = res.data;
            
            localStorage.setItem("jwtToken", token);
            localStorage.removeItem("user_phone_number");
            const decoded = jwt_decode(token);
            dispatch(setCurrentUser(decoded));
        })
        .catch(err => 
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
}

export const loginUserWithOtp = userData => dispatch => {
    axios
        .post('/login-with-otp', userData)
        .then(res => {
            const { token } = res.data;

            localStorage.setItem("jwtToken", token);
            
            const decoded = jwt_decode(token);
            dispatch(setCurrentUser(decoded));

        }).catch(err => 
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        );
}

// set logged in user

export const setCurrentUser = decoded => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    }
};

// user loading
export const setUserLoading = () =>{
    return {
        type: USER_LOADING
    }
}

// user logout

export const logoutUser = () => dispatch => {

    firebase.initializeApp(firebaseConfig);
    firebase.auth().signOut().then(function(){
        console.log('user signed out');
    }, function(err){
        console.log(err);
    })

    // Remove token from local storage
    localStorage.removeItem("jwtToken");
    localStorage.removeItem("user_id");
    Cookie.remove('jwtToken');
    // Remove auth header for future requests
    setAuthToken(false);
    // Set current user to empty object {} which will set isAuthenticated to false
    dispatch(setCurrentUser({}));
  };
