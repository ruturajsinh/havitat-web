import React,{Component} from 'react'
import firebaseConfig from '../firebaseConfig'
import * as firebaseui from "firebaseui"
import firebase from 'firebase'
import { Col, Row ,Modal } from 'react-bootstrap';

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUserWithOtp,registerWithOtp } from "../actions/authActions";

import Loading from '../components/Loading'

import axios from 'axios'

export class LoginWithOtp extends Component{

    constructor(props){
        super(props);

        this.state = {
            show: true,
            open_register_modal: false,
            name:'',
            email:'',
            password:'',
            c_password:'',
            user_phoneNumber:'',
            errors:{},
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.loading.show();
            window.location.href = "/create-property"; // push user to create property Page when they login
        }
        if (nextProps.errors) {
            console.log(nextProps.errors);
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    componentDidMount(){
        let self = this;
        const fbase = firebase.initializeApp(firebaseConfig);
        const uiConfig = {
          signInSuccessUrl: "/login-with-otp",
          signInOptions:[{
              provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
              recaptchaParameters:{
                type: 'image',
                size: 'invisible',
                badge: 'bottomleft'
              },
              defaultCountry:'PH'
          }],
          tosUrl:'/'
        }
        var ui = new firebaseui.auth.AuthUI(firebase.auth());
        
        ui.start('#firebaseui-auth-container', uiConfig);

        var initApp = function(){
            firebase.auth().onAuthStateChanged(function(user){
                if(user){
                    axios.get('/check-user-exits?number=' + user.phoneNumber).then(res => {
                        if(res.data.success){
                            console.log(res.data);
                            self.setState({ user_phoneNumber: user.phoneNumber });
                            self.alreadyHaveAccount();
                        }else{
                            self.setState({
                                show:false, 
                                open_register_modal: true, 
                                user_phoneNumber: user.phoneNumber 
                            });        
                        }
                    }).catch(err => { console.log(err); });
                }else{
                    console.log("ok now user signed out");
                }
            });
        }
        initApp();
        
    }
 
    clostRegisterModal = () => {
        this.setState({ open_register_modal: false });
        window.location.href = '/';
    }

    handleClose = () => {
        this.setState({ show: false });
        window.location.href = '/login';
    }

    alreadyHaveAccount(){
        const userData = {
            number: this.state.user_phoneNumber,
        }
        console.log('self');
        this.props.loginUserWithOtp(userData);
    }

    onSignInSubmit = e =>{
        e.preventDefault();
        const userData = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            c_password: this.state.c_password,
            number: this.state.user_phoneNumber
        }

        this.props.registerWithOtp(userData, this.props.history);
      }
    render(){
        const errors = this.state.errors;
        return(
            <div style={{"minHeight":'600px'}}>
            <Loading ref={loading => this.loading = loading}></Loading>
            <Modal
                show={this.state.show}
                onHide={this.handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div className="add-property-header">
                        <img src={require('../images/assets/logo_with_name.svg')} alt="logo name"></img>
                        <h6>Havitat</h6>
                    </div>
                    <div className="modal-orange-line"></div>
                    <div id="firebaseui-auth-container"></div>
                    <hr></hr>
                                <div className="other-option">
                                    <p>Or connect with :</p>
                                    <Row>
                                        <Col md={8} xs={8}>
                                            <img src={require('../images/icons/google.svg')} alt="google"></img>
                                            <span className="pl-2">Sign in with Google</span>
                                        </Col>
                                        <Col md={2} xs={2}>
                                            <div className="social-icon-box">
                                                <img src={require('../images/icons/facebook.svg')} alt="facebook"></img>
                                            </div>
                                        </Col>
                                        <Col md={2} xs={1}>
                                            <div className="social-icon-box">
                                                <img src={require('../images/icons/google.svg')} alt="mobile"></img>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                </Modal.Body>
            </Modal>
            <Modal
                show={this.state.open_register_modal}
                onHide={this.clostRegisterModal}
                backdrop="static"
                keyboard={false}
                >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div className="add-property-header">
                        <img src={require('../images/assets/logo_with_name.svg')} alt="logo name"></img>
                        <h6>Havitat</h6>
                    </div>
                    <div className="modal-orange-line"></div>
                    <h6 className="small-heading-title">Complete this step for registration</h6>
                    <form noValidate onSubmit={this.onSignInSubmit}>
                                    <div className="comman-input-area input-field">
                                        <input type="text" id="uniqe-name" value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }); }} required />
                                        <label htmlFor="uniqe-name">Name</label>
                                    </div>
                                    <span className="red-text"> {errors.name}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="uniqe-email" type="email" name="email" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }); }} autoComplete="new-password" required/>
                                        <label htmlFor="uniqe-email" >Enter Email</label>
                                    </div>
                                    <span className="red-text"> {errors.email}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="uniqe-password" type="password" name="password" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }); }} autoComplete="new-password" required />
                                        <label htmlFor="uniqe-password" >Password</label>
                                    </div>
                                    <span className="red-text"> {errors.password}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="confirm_password" type="password" name="confirm_password" value={this.state.c_password} onChange={(e) => { this.setState({ c_password: e.target.value }) }} required />
                                        <label htmlFor="confirm_password" >Confirm Password</label>
                                    </div>
                                    <span className="red-text"> {errors.c_password}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="mobile_number" type="text" name="mobile_number" value={this.state.user_phoneNumber} onChange={(e) => {this.setState({})}} required />
                                        <label htmlFor="mobile_number" >Mobile Number</label>
                                    </div>
                                    <div className="login-btn-area">
                                        <button type="submit" className="form-submit-btn">Register</button>
                                    </div>
                                    <div className="terms-of-use-text">
                                        <span>By Submitting, I accept Havitat <span className="orange-link-text-underline">Terms Of Use</span></span>
                                    </div>
                                    </form>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }

}

LoginWithOtp.propTypes = {
    registerWithOtp: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    { registerWithOtp,loginUserWithOtp }
)(LoginWithOtp);
