import React from 'react'
import { Col, Row, Container } from 'react-bootstrap';

class addProperty extends React.Component {
    
    constructor (props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state={
            file: null,
            preview_file: false
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event){
        this.setState({
            file: URL.createObjectURL(event.target.files[0]),
            preview_file: true
        });
    }

    handleClick(e) {
        this.refs.fileUploader.click();
    };

    removeImage = () => {
        this.setState({
            file: null,
            preview_file: false
        });
    };

    render (){
        
        return (
            <div id="listing_information">
                <input type='file' id='file' ref="fileUploader" style={{display: 'none'}} onChange={this.handleChange}/>
                <div id="LISTING_INFORMATION_u">
                    <span>LISTING INFORMATION</span>
                </div>
                <Container>
                    <Row>
                        <Col md={3} className="responsive-50-half-width">
                            <div className="comman-input-area input-field">
                                <select id="property-type" className="command-dropdown-with-border">
                                    <option value="">Monthly Rent</option>
                                </select>
                            </div>
                        </Col>
                        <Col md={3} className="responsive-50-half-width">
                        <div className="comman-input-area input-field">
                                <select id="property-type" className="command-dropdown-with-border">
                                    <option value="">Security Deposit</option>
                                </select>
                            </div>
                        </Col>
                        <Col md={3} className="responsive-50-half-width">
                        <div className="comman-input-area input-field">
                                <select id="property-type" className="command-dropdown-with-border">
                                    <option value="">Bedrooms</option>
                                </select>
                        </div>
                        </Col>
                        <Col md={3} className="responsive-50-half-width">
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border">
                                <option value="">Bathrooms</option>
                            </select>
                        </div>
                        </Col>
                        <Col md={3} className="responsive-50-half-width">
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border">
                                <option value="">Lease Duration</option>
                            </select>
                        </div>
                        </Col>
                        <Col md={3} className="responsive-50-half-width">
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border">
                                <option value="">Square feet</option>
                            </select>
                        </div>
                        </Col>
                        <Col md={3} className="responsive-50-half-width">
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border">
                                <option value="">Date available</option>
                            </select>
                        </div>
                        </Col>
                    </Row>
    
                    <Row className="comman-margin-top">
                        <Col md={6} lg={3}>
                            <div id="ac">
                                <div id="AMENITIES_RULES">
                                    <span>Amenities Rules</span>
                                </div>
                                <label className="checkbox-container pb-4">A/C
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Balcony or deck
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">FURNISHED
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">HardWood floor
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Garage parking
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Wheelchair access
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Off-street parking
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                            </div>
                        </Col>
                        <Col md={6} lg={3}>
                            <div id="ac">
                                <div id="AMENITIES_RULES">
                                    <span>Loundry</span>
                                </div>
                                <label className="checkbox-container pb-4">None
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">In Unit
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Shared or in-buliding
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                            </div>
                        </Col>
                        <Col md={6} lg={3}>
                            <div id="ac" className="rs-pt-5">
                                <div id="AMENITIES_RULES">
                                    <span>Pets</span>
                                </div>
                                <label className="checkbox-container pb-4">No pets allowed
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Cats ok
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Small Dogs ok
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                                <label className="checkbox-container pb-4">Large dogs ok
                                    <input type="checkbox"></input>
                                    <span className="checkmark"></span>
                                </label>
                            </div>
                        </Col>
                        <Col md={6} lg={3}>
                            <div id="ac" className="rs-pt-5">
                                <div id="AMENITIES_RULES">
                                    <span>AMENITIES NOT IN LIST ADD HERE</span>
                                </div>
                                <div id="select_">
                                <div className="comman-input-area input-field">
                                    <select id="property-type" className="command-dropdown-with-border">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                </div>
    
                                <div id="select_type">
                                    <div className="comman-input-area-with-border input-field">
                                        <input id="property-address" type="text" name="property-address" />
                                        <label htmlFor="property-address" className="input-label"></label>
                                    </div>
                                </div>
    
                                <div id="ADD_dh">
                                    <button className="form-submit-small-btn" >Add</button>
                                </div>
    
                            </div>
                        </Col>
                    </Row>
    
                    <Row className="comman-margin-top">
                    <div id="LISTING_INFORMATION_u">
                        <span>DETAILED DESCRIPTION</span>
                    </div>
    
                    <div id="about_the_property">
                        <label htmlFor="about_property" className="text-area-label">About the property</label>
                        <textarea id="about_property" className="long_textArea" placeholder="Ex:New home with fresh furniture with items."></textarea>
                    </div>
    
                    <div id="about_the_property">
                        <label htmlFor="about_property" className="text-area-label">Lease summary</label>
                        <textarea className="long_textArea" placeholder="Ex:New home with fresh furniture with items."></textarea>
                    </div>
                    </Row>
                    <Row className="comman-margin-top">
                    <div id="LISTING_INFORMATION_u">
                        <span>PROPERTY MEDIA <img src={require('../images/assets/add.svg')} alt="add" /> </span>
                    </div>
                    
                        <Col md={4}>
                            <img className="comman-img-class" src={require('../images/image__01.png')} alt="comman-1"></img>
                        </Col>
                        <Col md={4}>
                            <img className="comman-img-class" src={require('../images/image__02.png')} alt="comman-2"></img>
                        </Col>
                        <Col md={4}>
                            <img className="comman-img-class" src={require('../images/image__03.png')} alt="comman-3"></img>
                        </Col>
                        <Col md={4}>
                        <div id="upload_photo" onClick={this.handleClick}>
                            <div className="Rectangle__dw">
                                <div id="Rectangle__dw">
                                    <img id="upload" src={require('../images/assets/upload.svg')} alt="upload"></img>
                                    <div id="Upload_Photo__text">
                                    <span>Upload Photo</span>
                                </div>
                                </div>
                            </div>
                        </div>
                        </Col>
                        { this.state.preview_file ? 
                        <Col md={4}>
                            <div className="preview-image" onClick={this.removeImage}>
                                <div className="close-preview-img"><i className="fa fa-times"></i></div>
                                <img src={this.state.file}/>
                            </div>
                        </Col> : ""  }
                        
                    </Row>
    
                    <Row className="comman-margin-top">
                        <div id="LISTING_INFORMATION_u">
                            <span>VIEW MAP</span>
                        </div>
                        <Col xs={12}>
                        <iframe title="map_view" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.3060207141!2d-74.2598715610934!3d40.69714940463053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1601636283261!5m2!1sen!2sin" height="450" frameBorder="0" style={{border:'0',width:'100%'}} allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>
                        </Col>
                    </Row>
                </Container>
    
            </div>
        );
    }
}

export default addProperty;