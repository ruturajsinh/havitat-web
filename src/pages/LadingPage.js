import React, { Component } from 'react'

import pexelsBinyamin from '../images/pexels-binyamin-mellish.png'
import SearchIcon from '../images/search-icon.svg'
import home1 from '../images/home-1.png'
import image2 from '../images/image-2.png'

import image01 from '../images/01.png'
import image02 from '../images/02.png'
import image03 from '../images/03.png'
import image04 from '../images/04.png'
import image05 from '../images/05.png'
import image06 from '../images/06.png'
import image07 from '../images/07.png'

import { Col, Row, Container } from 'react-bootstrap'

class LadingPage extends Component {

	constructor(props){
		super(props);

		this.state = {
			showRent: false,
			type: 'Sale',
			search_value:''
		}
	}
	
	rentClick = type => () => {
		if(type === "rent"){
			this.setState({ showRent: true, type: 'Rent' });
		}else if(type === "sell"){
			this.setState({ showRent: false, type: 'Sale' });
		}
	}

	search = () => {
		let type = this.state.type;
		localStorage.setItem("search_text",this.state.search_value);
		if(type === 'Sale'){
			window.location.href = '/sell';
		}else{
			window.location.href = '/rent';
		}
	}

	render(){
	const showRent = this.state.showRent;
	

	const Sell = () => (
		<div id="rentsale">
			<div id="sale">
				<div className="Rectangle_184" onClick={this.rentClick("rent")}>
					<div id="Rectangle_184">
						<div id="Rent">
							<span>Rent</span>
						</div>
					</div>
				</div>
				<div className="Rectangle_166">
					<div id="Rectangle_166">
						<div id="Sell">
							<span>Sell</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	)

	const Rent = () => (
		<div id="rentsale">
			<div id="sale">
				<div className="Rectangle_166" onClick={this.rentClick("rent")}>
					<div id="Rectangle_166">
						<div id="Rent">
							<span>Rent</span>
						</div>
					</div>
				</div>
				<div className="Rectangle_166" onClick={this.rentClick("sell")}>
					<div id="Rectangle_184">
						<div id="Sell">
							<span>Sell</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	)

	return (
		<>
		<Container fluid>
			<div id="find_your_perfect_home">
				<div id="image_dk">
					<img id="pexels-binyamin-mellish-106399" src={pexelsBinyamin} alt="pexelsBinyamin" ></img>
					<div className="Rectangle_160">
						<div id="Rectangle_160"></div>
					</div>
				</div>
				<div id="Find_Your_Parfect_Home">
					<span>Find Your Perfect Home</span>
				</div>
				{showRent ? <Rent /> : <Sell />}
				<div id="search">
					<input type="text" name="search" value={this.state.search_value} 
					onChange={ (e) => { this.setState({ search_value: e.target.value }) } } 
					onKeyPress={ (e) => {
						if(e.key === 'Enter'){
							this.search();
						}
					} } id="Rectangle_164"></input>
					<button id="Rectangle_165" onClick={this.search}><img id="magnifying-glass" src={SearchIcon} alt="search-icon-2" ></img></button>
				</div>
			</div>
		</Container>

			<div id="explore_neighborhood_on_havita">
				<div id="Take_a_deep_dive_and_browse_or">
					<div id="Explore_neighborhoods_on_Havit">
						<span>Explore Neighbourhood On Havitat</span>
						<div className="Rectangle_4">
							<div className="title-underline"></div>
						</div>
					</div>
					<p className="comman-para">
						Take a deep dive and browse original neighbourhood photos,drone,footage,resident reviews <br />
						insights to see if the homes or a apartments for rent right for you.
                	</p>
				</div>
				<Container className="comman-margin-top">
					<Row className="text-center">
						<Col md={6}>
							<img id="home-1" src={home1} alt="home-1" ></img>
						</Col>
						<Col md={6}>
							<img id="image-2" src={image2} alt="home-2" ></img>
						</Col>
					</Row>
				</Container>
			</div>

			<div id="Best_Properties_to_buy_in">
				<div id="text-best_properties_to_buy">
					<div id="Best_properties_To_buy_in_Maka">
						<span>Best properties To buy in Makati & BGC </span>
						<div className="Rectangle_4">
							<div className="title-underline"></div>
						</div>
					</div>
					<div id="Explore_the_new_properties_ava">
						<span>Explore the new properties available for sale in your surrounding.</span>
					</div>
				</div>
				<div id="image">
					<a href="/property-details">
						<div className="image-1">
							<img id="ID01" src={image01} alt="id01"></img>
							<div id="Atlanta_GA__Downtown">
								<span>Atlanta ,GA</span><br />
								<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>Downtown</span>
							</div>
						</div>
					</a>
					<div id="image-2_ba">
						<img id="ID02" src={image02} alt="id02"></img>
						<svg className="Rectangle_209">
							<rect id="Rectangle_209" rx="15" ry="15" x="0" y="0" width="351" height="200">
							</rect>
						</svg>
						<div id="ScottsdaleAZ__North_Scottsdale">
							<span>Scottsdale,AZ</span><br />
							<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>North Scottsdale</span>
						</div>
					</div>
					<div id="image-3">
						<img id="ID03" src={image03} alt="ID03"></img>
						<svg className="Rectangle_210">
							<rect id="Rectangle_210" rx="15" ry="15" x="0" y="0" width="351" height="200">
							</rect>
						</svg>
						<div id="Austin__TX_Rosedale">
							<span>Austin , TX</span><br />
							<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>Rosedale</span>
						</div>
					</div>
					<div id="image-4">
						<img id="ID04" src={image04} alt="ID04"></img>
						<svg className="Rectangle_214">
							<rect id="Rectangle_214" rx="15" ry="15" x="0" y="0" width="351" height="433">
							</rect>
						</svg>
						<div id="Boston_MA_Hyde_Park">
							<span>Boston, MA</span><br />
							<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>Hyde Park</span>
						</div>
					</div>
					<div id="image-5">
						<img id="ID05" src={image05} alt="ID05"></img>
						<svg className="Rectangle_212">
							<rect id="Rectangle_212" rx="15" ry="15" x="0" y="0" width="351" height="200">
							</rect>
						</svg>
						<div id="Oakland_CA_Longfellow">
							<span>Oakland, CA</span><br />
							<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>Longfellow</span>
						</div>
					</div>
					<div id="image-6">
						<img id="ID06" src={image06} alt="ID06"></img>
						<svg className="Rectangle_211">
							<rect id="Rectangle_211" rx="15" ry="15" x="0" y="0" width="351" height="200">
							</rect>
						</svg>
						<div id="Newton__MA_Chestnut_Hill">
							<span>Newton , MA</span><br />
							<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>Chestnut Hill</span>
						</div>
					</div>
					<div id="image-7">
						<img id="ID07" src={image07} alt="ID047"></img>
						<svg className="Rectangle_213">
							<rect id="Rectangle_213" rx="15" ry="15" x="0" y="0" width="351" height="443">
							</rect>
						</svg>
						<div id="Sandy_Sprin_North_Spring">
							<span>Sandy Sprin</span><br />
							<span style={{ 'fontStyle': 'normal', 'fontWeight': '500', 'fontSize': '35px' }}>North Spring</span>
						</div>
					</div>
				</div>
				<div id="up">
					<img id="up-chevron" src={require('../images/up.png')} alt="up-chevron"></img>
				</div>
			</div>

			<div id="havitat_knowledge_base" className="comman-margin-top">
				<Container fluid>
					<Row>
						<Col md={6}>
							<div id="havitat_knowledge_base_cj">
								<div id="_Havitat_Knowledge_Base_______">
									<span style={{ 'fontFamily': 'Segoe UI', 'fontStyle': 'normal', 'fontWeight': '500' }}>Havitat Knowledge Base</span>
								</div>
								<div id="Everything_you_need_know_when_">
									<span>Everything you need know<br />when looking at <br />Homes for rent all in one place.<br /></span><br />
								</div>
								<div className="Rectangle_8">
									<button id="Rectangle_8" className="" type="button">See all the guides</button>
								</div>
							</div>
						</Col>
						<Col md={6}>
							<Row className="mobile-margin-top">
								<Col md={6}>
									<div id="renter_home">
										<img id="how-to-write-a-winning-offer-l" src={require('../images/how-to-write-a-winning-offer-l.svg')} alt="wining-offer" ></img>
										<div id="RENTER_A_HOME">
											<span>RENTER A HOME</span>
										</div>
										<div id="With_35filters_ana_custom_Keyw_ch">
											<span>With 35+filters ana custom Keywword search, <br />
							Trulia can help you home for rent that you'll love.</span>
										</div>
									</div>
								</Col>
								<Col md={6}>
									<div id="renter_home">
										<img id="how-to-write-a-winning-offer-l" src={require('../images/buyer.svg')} alt="buyer"></img>
										<div id="RENTER_A_HOME">
											<span>BUYER A HOME</span>
										</div>
										<div id="With_35filters_ana_custom_Keyw_ch">
											<span>With 35+filters ana custom Keywword search,<br />Trulia can help you home for rent that you'll love.</span>
										</div>
									</div>
								</Col>
								<Col md={6}>
									<div id="renter_home">
										<img id="how-to-write-a-winning-offer-l" src={require('../images/seller-home.svg')} alt="seller-home"></img>
										<div id="RENTER_A_HOME">
											<span>SELLER A HOME</span>
										</div>
										<div id="With_35filters_ana_custom_Keyw_ch">
											<span>With 35+filters ana custom Keywword search,<br />Trulia can help you home for rent that you'll love.</span>
										</div>
									</div>
								</Col>
								<Col md={6}>
									<div id="renter_home">
										<img id="how-to-write-a-winning-offer-l" src={require('../images/lesser-home.svg')} alt="lesser"></img>
										<div id="RENTER_A_HOME">
											<span>LESSER A HOME</span>
										</div>
										<div id="With_35filters_ana_custom_Keyw_ch">
											<span>With 35+filters ana custom Keywword search,<br />
							Trulia can help you home for rent that you'll love.</span>
										</div>
									</div>
								</Col>
							</Row>
						</Col>
					</Row>

				</Container>

			</div>

		</>
	);
	}
	
}

export default LadingPage;