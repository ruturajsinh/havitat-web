import React from 'react'
import { Col, Row, Container } from 'react-bootstrap';

import InputRange from 'react-input-range';
import axios from 'axios'
import isEmpty from 'is-empty'
import config from '../config'

import Loading from '../components/Loading'

class Sell extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            value: 41,
            propertyApartmentType: false,
            sortByPriceLtoH: false,
            mapview: false,
            sale_property_lists: [],
            search_value: localStorage.getItem("search_text") ? localStorage.getItem("search_text") : '',
            filter: 'HtoL',
            property_sub_type:'House'
        };
        this.showLtoH = this.showLtoH.bind(this);
    }

    componentWillMount() {
        this.getSalePropertyList();
    }

    getSalePropertyList() {
        axios.get('/get-sale-property?search='+ this.state.search_value +'&sub_type='+this.state.property_sub_type + '&filter=' + this.state.filter).then(res => {
            this.setState({ sale_property_lists: res.data.data });
            localStorage.removeItem("search_text");
        }).catch(err => { console.log(err); });
    }

    showApartment = propertyType => () => {
        if (propertyType === "apartment") {
            this.setState({
                propertyApartmentType: true
            })
        } else if (propertyType === "house") {
            this.setState({
                propertyApartmentType: false
            });
        }
    }

    showMapView = viewType => () => {
        if (viewType === "map") {
            this.setState({
                mapview: true,
            });
            
        } else if (viewType === "list") {
            this.setState({
                mapview: false,
            });
            
        }
    }
    
    showLtoH = type => () => {
        this.setState({
            filter: type
        });
        console.log(type);
        this.filterProperty(type,this.state.property_sub_type);
    }

    handleSelectChange =(e)=>{
        let selected_value = e.target.value
        this.setState({ property_sub_type: selected_value });
        this.filterProperty(this.state.filter,e.target.value);
    }


    filterProperty = (filter,sub_type) => {
        this.loading.show();
        
        let search = this.state.search_value;
        let property_sub_type = sub_type;
        
        axios.get('/get-sale-property?search=' + search + '&filter=' + filter + '&sub_type=' + property_sub_type).then(res => {
            this.setState({ sale_property_lists: res.data.data });
            this.loading.hide();
        }).catch(err => { console.log(err); this.loading.hide(); });
    }


    render() {

        const listItems = this.state.sale_property_lists;

        const ListView = () => (

            <Row className="property-lists">

                { !isEmpty(listItems) ?

                    listItems.map((listItem) =>
                        <Col md={6} lg={3} key={listItem._id}>
                            <a href="/property-details">
                                <div id="image_part-1">
                                    <div id="image-22">
                                        <div className="Rectangle_215">

                                            <div id="house_of_rent_-_text">
                                                <img id="image_2" src={config.image_url + listItem.property_images[0]} alt="pexels-1298"></img>
                                            </div>
                                            <div id="text">
                                                <div id="House_of_Rent">
                                                    <img id="gps" src={require('../images/gps.svg')} alt="gps-normal"></img> <span>{listItem.property_title}</span>
                                                </div>

                                                <div id="Kamuning_Quezon_City">
                                                    <span>{listItem.property_address}</span>
                                                    <span id="_2100">${listItem.offer_price.offer_price}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </a>
                        </Col>
                    )
                    :
                    <div className="data-not-found-alert-box">
                        <div className="data-not-found-text">
                            <p>Sorry, We haven't any Sell Property yet. Please Come back after few time.</p>
                        </div>
                    </div>
                }
            </Row>
        )
        const MapView = () => (
            <Row className="property-lists">
                <Col md={6} lg={6}>
                    <Row>
                        {listItems.map((listItem) =>
                            <Col md={12} lg={6} key={listItem}>
                                <a href="/property-details">
                                    <div id="image_part-1">
                                        <div id="image-22">
                                            <div className="Rectangle_215">

                                                <div id="house_of_rent_-_text">
                                                    <img id="image_2" src={config.image_url + listItem.property_images[0]} alt="pexels-1298"></img>
                                                </div>
                                                <div id="text">
                                                    <div id="House_of_Rent">
                                                        <img id="gps" src={require('../images/gps.svg')} alt="gps-normal"></img> <span>{listItem.property_title}</span>
                                                    </div>

                                                    <div id="Kamuning_Quezon_City">
                                                        <span>{listItem.property_address}</span>
                                                        <span id="_2100">${listItem.offer_price.offer_price}</span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </Col>
                        )}
                    </Row>
                </Col>
                <Col md={6} lg={6}>
                    <iframe title="map_view" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.3060207141!2d-74.2598715610934!3d40.69714940463053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1601636283261!5m2!1sen!2sin" className="responsive-map-view" frameBorder="0" style={{ border: '0', width: '100%', height: '50%' }} allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>
                </Col>
            </Row>
        )

        return (

            <section>
                <Loading ref={loading => this.loading = loading}></Loading>
                <div id="search_property">
                    <input type="text" className="search-property-input" name="search" value={this.state.search_value} onChange={ (e) => { this.setState({ search_value: e.target.value }) } } onKeyPress={
                        event => {
                            if(event.key === 'Enter'){
                                this.filterProperty(this.state.filter,this.state.property_sub_type);
                            }
                        }
                    }></input>
                    <button className="search-property-btn" onClick={this.filterProperty.bind(this,this.state.filter,this.state.property_sub_type)}><img id="magnifying-glass-property" src={require('../images/search-icon.svg')} alt="search-icon"></img></button>
                </div>
                <Container fluid>
                    <Row className="search-property-filter">
                        <Container>
                            <Row>
                                <Col md={6} lg={3}>
                                    <div id="property_type">
                                        <div id="Property_type_be">
                                            <span>Property type</span>
                                        </div>
                                        <div className="comman-input-area input-field">
                                        <select id="property-type" className="command-dropdown-with-border" onChange={this.handleSelectChange.bind(this)}  value={this.state.property_sub_type} >
                                            <option value="House">House</option>
                                            <option value="Villa">Villa</option>
                                            <option value="Bunglow">Bunglow</option>
                                        </select>
                                        </div>
                                    </div>
                                </Col>
                                <Col md={6} lg={4}>
                                    <div id="property_type">
                                        <div id="Property_type_be">
                                            <span>Sort by Price</span>
                                        </div>
                                        <button className={this.state.filter === 'HtoL' ? 'comman-orange-btn ml-2' : 'white-comman-btn'} onClick={this.showLtoH("HtoL")}>High to Low</button>
                                        <button className={this.state.filter === 'LtoH' ? 'comman-orange-btn ml-2' : 'white-comman-btn ml-2'} onClick={this.showLtoH("LtoH")}>Low to High</button>
                                    </div>
                                </Col>

                                <Col md={6} lg={3}>
                                    <div id="property_type">
                                        <div id="Property_type_be">
                                            <span>Sort by Distance</span>
                                        </div>

                                        <form className="form">
                                            <InputRange
                                                maxValue={150}
                                                minValue={5}
                                                value={this.state.value}
                                                onChange={value => this.setState({ value })}
                                                onChangeComplete={value => console.log(value)} />
                                        </form>
                                    </div>
                                </Col>
                                <Col md={6} lg={2}>
                                    {
                                        this.state.mapview ?
                                            <div id="list_map">
                                                <div className="Rectangle_181" onClick={this.showMapView("list")}>
                                                    <div id="Rectangle_181" className="Rectangle_232_color">
                                                        <img id="list_4" src={require('../images/list_orange.svg')} alt="list-4"></img>
                                                    </div>
                                                </div>
                                                <div className="Rectangle_232" onClick={this.showMapView("map")}>
                                                    <div id="Rectangle_232" className="Rectangle_181_color">
                                                        <img id="gps_fa" src={require('../images/gps_white.svg')} alt="gps-fa"></img>
                                                    </div>
                                                </div>
                                            </div>
                                            :
                                            <div id="list_map">
                                                <div className="Rectangle_181" onClick={this.showMapView("list")}>
                                                    <div id="Rectangle_181" className="Rectangle_181_color">
                                                        <img id="list_4" src={require('../images/list.svg')} alt="list-45"></img>
                                                    </div>
                                                </div>
                                                <div className="Rectangle_232" onClick={this.showMapView("map")}>
                                                    <div id="Rectangle_232" className="Rectangle_232_color">
                                                        <img id="gps_fa" src={require('../images/gps.svg')} alt="gps-fa-white"></img>
                                                    </div>
                                                </div>
                                            </div>
                                    }

                                </Col>
                            </Row>
                        </Container>
                    </Row>
                    <Row>
                        <Col>
                            <div id="Apartments_for_Sell_I_Manila__">
                                <span>Apartments for Sell I Manila , PH</span>
                            </div>
                            <div id="ID4172_Rentals_available_on_Ha">
                                <span>4,172 Rentals available on Havitat</span>
                            </div>
                        </Col>
                    </Row>
                    {this.state.mapview ? <MapView /> : <ListView />}
                </Container>

            </section>
        );
    };
}

export default Sell;