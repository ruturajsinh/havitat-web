import React, { Component } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import axios from 'axios'
import config from '../config'
import SuccessModal from '../components/modal/SuccessModal'
import { confirmAlert } from 'react-confirm-alert'
import isEmpty from 'is-empty';

class ManageMyProperty extends Component {

    constructor(props){
        super(props);

        this.state = {
            rent_lists:[],
            sell_lists:[],
            user_id: localStorage.getItem("user_id")
        };
    }
    
    componentWillMount(){
        this.getMyRentProperties();
        this.getMySaleProperties();
    }

    getMyRentProperties(){
        axios.get('/get-user-properties/' + this.state.user_id + '?types=For%20Rent').then(res => {
            this.setState({ rent_lists: res.data.data });
        }).catch(err => { console.log(err); });
    }

    getMySaleProperties(){
        axios.get('/get-user-properties/' + this.state.user_id + '?types=For%20Sale').then(res => {
            this.setState({ sell_lists: res.data.data });
        }).catch(err => { console.log(err); });
    }

    submitForDelete = (property_id) => {
        confirmAlert({
            title: 'Havitat',
            message: 'Are you sure want to delete this Property?',
            buttons:[
                {
                    label:'Yes',
                    onClick: () => this.deleteRentProperty(property_id)
                },
                {
                    label:'No'
                }
            ]
        })
    };

    deleteRentProperty = (property_id) => {
        axios.post('/delete-rent-property/' +  property_id).then(res => {
            this.success.handleShow();
            this.success.successText(res.data.message);
            this.success.setRedirectUrl('/manage-my-property');
        }).catch(err => { console.log(err); });
    }

    submitForSaleDelete = (proeprty_id) =>{
        confirmAlert({
            title: 'Havitat',
            message: 'Are you sure want to delete this Property?',
            buttons:[
                {
                    label:'Yes',
                    onClick: () => this.deleteSaleProperty(proeprty_id)
                },
                {
                    label:'No'
                }
            ]
        })
    };

    deleteSaleProperty = (proeprty_id) => {
        axios.post('/delete-sale-property/' + proeprty_id ).then(res => {
            this.success.handleShow();
            this.success.successText(res.data.message);
            this.success.setRedirectUrl('/manage-my-property');
        }).catch(err => { console.log(err); });
    }

    render(){

        const RentProperties = this.state.rent_lists;
        const SaleProperties = this.state.sell_lists;

        return (
            <Container className="responsive-bottom-pb-10">
                <SuccessModal ref={success => this.success = success}></SuccessModal>
                <Row className="comman-margin-top responsive-container">
                    <div className="left-side-title"><span>My Listing</span></div>
                </Row>
                <Row className="comman-margin-top responsive-container">
                    <div className="left-side-title">
                        <span>Rent</span>
                        <div className="orange-line"></div>
                    </div>
                    <Row className="comman-margin-top">
                        {   !isEmpty(RentProperties) ?
                        
                        RentProperties.map((rent_property) => 
                        <Col md={3} key={rent_property._id}>
                        <div id="image_part-1">
                            <div id="image-22">
                                <div className="Rectangle_215">

                                    <div id="house_of_rent_-_text">
                                        <img id="image_2" src={config.image_url + rent_property.property_images[0]} alt="pexels-1298"></img>
                                    </div>
                                    <div id="text">
                                        <div id="House_of_Rent">
                                            <img id="gps" src={require('../images/gps.svg')} alt="gps-normal"></img> <span>{rent_property.property_title}</span>
                                        </div>

                                        <div id="Kamuning_Quezon_City">
                                            <span>{rent_property.property_address.substring(0,35) + '...'}</span>
                                            <span id="_2100">${rent_property.offer_price.offer_price}</span>
                                        </div>

                                    <div className="edit-or-delete">
                                        <div className="edit-property">
                                            <a className="currsor-pointer" href={"edit-property/" + rent_property._id }>
                                                <i className="fa fa-pencil-square-o"></i>
                                            </a>
                                        </div>
                                        <div className="delete-property currsor-pointer" >
                                            <i className="fa fa-trash-o" onClick={ () => this.submitForDelete(rent_property._id) }></i>
                                        </div>
                                    </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        </Col>
                        )
                    :
                    <Col lg={12}>
                        <div className="data-not-found-alert-box">
                        <div className="data-not-found-text">
                            <p>You haven't added any property for rent yet. Click on Add New for add new Property</p>
                        </div>
                        </div>
                    </Col>
                    }        
                    </Row>
                        <Col lg={12}>
                        <a href="/create-property">
                            <div className="add_new_property_checkbox">
                                <img src={require('../images/assets/plus.svg')} alt="plus" /><div className="checkbox-text"><span>Add New</span></div>
                            </div>
                        </a>
                        </Col>
                </Row>

                <Row className="comman-margin-top responsive-container">
                    <div className="left-side-title">
                        <span>Sell</span>
                        <div className="orange-line"></div>
                    </div>
                    <Row className="comman-margin-top">
                    {   !isEmpty(SaleProperties) ?
                        SaleProperties.map((sale_property) => 
                        <Col md={3} key={sale_property._id}>
                            <div id="image_part-1">
                            <div id="image-22">
                                <div className="Rectangle_215">

                                    <div id="house_of_rent_-_text">
                                        <img id="image_2" src={config.image_url + sale_property.property_images[0]} alt="pexels-1298"></img>
                                    </div>
                                    <div id="text">
                                        <div id="House_of_Rent">
                                            <img id="gps" src={require('../images/gps.svg')} alt="gps-normal"></img> <span>{sale_property.property_title}</span>
                                        </div>

                                        <div id="Kamuning_Quezon_City">
                                            <span>{sale_property.property_address.substring(0,35) + '...'}</span>
                                            <span id="_2100">${sale_property.offer_price.offer_price}</span>
                                        </div>
                                        <div className="edit-or-delete">
                                            <div className="edit-property">
                                                <a className="currsor-pointer" href={"edit-property/" + sale_property._id }>
                                                    <i className="fa fa-pencil-square-o"></i>
                                                </a>
                                            </div>
                                            <div className="delete-property currsor-pointer" >
                                                <i className="fa fa-trash-o" onClick={ () => this.submitForSaleDelete(sale_property._id) }></i>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        </Col>
                        )
                    : 
                    <Col lg={12}>
                        <div className="data-not-found-alert-box">
                        <div className="data-not-found-text">
                            <p>You haven't added any property for sale yet. Click on Add New for add new Property</p>
                        </div>
                        </div>
                    </Col>
                    }
                    </Row>
                    <Col lg={12}>
                    <a href="/create-property">
                        <div className="add_new_property_checkbox">
                            <img src={require('../images/assets/plus.svg')} alt="plus-03" /><div className="checkbox-text"><span>Add New</span></div>
                        </div>
                    </a>
                    </Col>
                </Row>

            </Container>
        );
    }
        
}

export default ManageMyProperty;