import React from 'react'
import { Col, Row, Container } from 'react-bootstrap';

function PropertyDetails() {
    return (
        <section>
            <Container>
                <div id="image_part-3" className="comman-margin-top">
                    <Row>
                        <Col md={8}>
                            <div id="MALE_BEDSPACE_IN_KAMUNING">
                                <span>MALE BEDSPACE IN KAMUNING</span>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div id="_2000_">
                                <span>$ 2000</span>
                            </div>
                        </Col>

                    </Row>

                    <div id="Kamuning_Quezon_City_id">
                        <span><img id="gps_1" src={require('../images/gps.svg')} alt="gps"></img> Kamuning, Quezon City</span>
                    </div>
                    <Row>
                        <Col md={8}>
                            <img id="image_123" src={require('../images/image_1.png')} alt="gps-2"></img>
                        </Col>
                        <Col md={4}>
                            <Row>
                                <Col md={12}>
                                    <img id="image_323" src={require('../images/image_2.png')} alt="gps-323"></img>
                                </Col>
                                <Col md={12}>
                                    <img id="image_323" src={require('../images/image_3.png')} alt="gps-123"></img>
                                </Col>
                            </Row>
                        </Col>
                    </Row>


                    <div id="home_details">
                        <div id="Home_Details_be">
                            <span>Home Details</span>
                        </div>
                        <Row>
                            <Col sm={2} className="responsive-half-width">
                                <div id="condo">
                                    <img id="condo_bg" src={require('../images/assets/condo.svg')} alt="condo"></img>
                                    <div id="condo_bh">
                                        <span>condo</span>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={2} className="col-half-offset responsive-half-width">
                                <div id="condo">
                                    <img id="bed" src={require('../images/assets/bed.svg')} alt="bed"></img>
                                    <div id="condo_bh">
                                        <span>4 Beds</span>
                                    </div>
                                </div>
                            </Col>

                            <Col sm={2} className="col-half-offset responsive-half-width">
                                <div id="condo">
                                    <img id="bath_bm" src={require('../images/assets/bath.svg')} alt="bath"></img>
                                    <div id="condo_bh">
                                        <span>2 Baths</span>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={2} className="col-half-offset responsive-half-width">
                                <div id="condo">
                                    <img id="vehicle" src={require('../images/assets/mp.svg')} alt="mp"></img>
                                    <div id="condo_bh">
                                        <span>2180 Sq Ft</span>
                                    </div>
                                </div>
                            </Col>
                            <Col sm={2} className="col-half-offset responsive-half-width">
                                <div id="condo">
                                    <img id="vehicle" src={require('../images/assets/vehicle.svg')} alt="vehicle"></img>
                                    <div id="condo_bh">
                                        <span>1 Garage</span>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>

                    <Row className="comman-margin-top">
                        <Col md={12} lg={7}>
                            <div id="amenity_details">
                                <div id="Amenity_Details_">
                                    <span>Amenity Details </span>
                                </div>
                                <Row className="amenity_details">
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Basement
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Doorman
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Hardwood flooring
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Elevator
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Parking
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Car Garage
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Cleaning Service
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Family Room
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>
                                    <Col md={4} lg={4} className="responsive-half-width">
                                        <label className="checkbox-container pb-4">
                                            <input type="checkbox"></input>Pets allowed
                                            <span className="checkmark"></span>
                                        </label>
                                    </Col>

                                    <Col md={12}>
                                        <div id="property_details">
                                            <div id="Property_Details_cq">
                                                <span>Property Details</span>
                                            </div>
                                            <div id="A_private_bedroom_is_available">
                                                <span>A private bedroom is available at this gorgeous 5 bed , 2 bath Crown Heights apartment. The interior <br />
                                                boasts ark hardwood floors, exposed brick, a skylight, and stainless steel appliances  This home features
                                                <br />furnished common spaces , A/C, in-unit laundry,and kitchen essentials. It also provides  high-speed WiFi
                                                <br />and monthly professional cleaning services. Parking is available on a first come, first served basis </span>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                        <Col md={12} lg={5}>
                            <div id="contact_property_owneragent">
                                <div id="CONTACT_PROPERTY_OWNERAGENT_">
                                    <span>CONTACT PROPERTY OWNER/AGENT</span>
                                </div>
                                <div className="quote-form-fileds">
                                    <div className="comman-input-area-with-border input-field">
                                        <input id="name" type="text" name="name" required/>
                                        <label htmlFor="name" className="input-label">Your Name</label>
                                    </div>

                                    <div className="comman-input-area-with-border input-field">
                                        <input id="email" type="email" name="email" required/>
                                        <label htmlFor="email" className="input-label">Email Address</label>
                                    </div>

                                    <div className="comman-input-area-with-border input-field">
                                        <input id="number" type="tel" name="number" required/>
                                        <label htmlFor="number" className="input-label">Contact Number</label>
                                    </div>

                                    <div className="login-btn-area">
                                        <button className="form-submit-btn">Submit</button>
                                    </div>

                                    <div id="or_contact_us_on_WhatsAppviber">
                                        <span>or contact us on WhatsApp/viber</span>
                                    </div>

                                    <div className="social-contact-wp">
                                        {/* <a href="//api.whatsapp.com/send?phone=91{phone_number}&text=Inquiry for property sell or rent"> */}
                                            <img id="whatsapp" src={require('../images/assets/whatsapp.svg')} alt="whatsapp"></img>
                                        {/* </a> */}
                                        <img id="messenger" src={require('../images/assets/messenger.svg')} alt="messanger"></img>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>

                    <div className="Neighborhood-Description comman-margin-top">
                        <div id="Home_Details_be">
                            <span>Neighbourhood Description</span>
                        </div>
                        <Row>

                            <Col md={6} sm={6} lg={4}>
                                <Row>
                                    <Col md={6} lg={6} className="responsive-half-width">
                                        <img id="tony-lee-8IKf54pc3qk-unsplash" src={require('../images/tony-lee-8IKf54pc3qk-unsplash.png')} alt="tony-1"></img>
                                    </Col>
                                    <Col md={6} lg={6} className="responsive-half-width">
                                        <img id="tony-lee-8IKf54pc3qk-unsplash" src={require('../images/egor-kosten--zx5KO1bxmg-unspla.png')} alt="tony-2"></img>
                                    </Col>
                                    <Col md={6} lg={6} className="responsive-half-width">
                                        <img id="tony-lee-8IKf54pc3qk-unsplash" src={require('../images/pexels-elina-sazonova-1850592.png')} alt="tony-3"></img>
                                    </Col>
                                    <Col md={6} lg={6} className="responsive-half-width">
                                        <img id="tony-lee-8IKf54pc3qk-unsplash" src={require('../images/pexels-tuur-tisseghem-2954405.png')} alt="tony-4"></img>
                                    </Col>
                                    <Col md={6} lg={6} className="responsive-half-width">
                                        <img id="tony-lee-8IKf54pc3qk-unsplash" src={require('../images/pexels-jonathan-meyer-668300.png')} alt="tony-5"></img>
                                    </Col>
                                    <Col md={6} lg={6} className="responsive-half-width">
                                        <img id="tony-lee-8IKf54pc3qk-unsplash" src={require('../images/pexels-craig-adderley-2566121.png')} alt="tony-6"></img>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={6} sm={6} lg={8}>
                                <iframe title="map_view" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.3060207141!2d-74.2598715610934!3d40.69714940463053!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew%20York%2C%20NY%2C%20USA!5e0!3m2!1sen!2sin!4v1601636283261!5m2!1sen!2sin" frameBorder="0" style={{border:'0',width:'100%', height: '100%'}} allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>
                            </Col>
                        </Row>
                    </div>

                    <div id="schoolretail">
                        <div id="SchoolRetailTransportation">
                            <span>School/Retail/Transportation</span>
                        </div>
                        <Row>
                            <Col md={6} lg={3} className="resposnive-pb-10">
                                <div id="school">
                                    <div className="Rectangle_125">
                                        <div id="Rectangle_125">
                                            <img id="school_dr" src={require('../images/assets/school.svg')} alt="school"></img> School
                                    </div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={6} lg={3} className="resposnive-pb-10">
                                <div id="school">
                                    <div className="Rectangle_125">
                                        <div id="Rectangle_125">
                                            <img id="school_dr" src={require('../images/assets/food.svg')} alt="hotel"></img> Hotel &amp; Restaurants
                                    </div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={6} lg={3} className="resposnive-pb-10">
                                <div id="school">
                                    <div className="Rectangle_125">
                                        <div id="Rectangle_125">
                                            <img id="school_dr" src={require('../images/assets/bus.svg')} alt="transportation"></img> transportations
                                    </div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={6} lg={3}>
                                <div id="school">
                                    <div className="Rectangle_125">
                                        <div id="Rectangle_125">
                                            <img id="school_dr" src={require('../images/assets/bus.svg')} alt="shopping malls"></img> Shopping Malls &amp; Stores
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>

                    <div className="listin-for-rent comman-margin-top">
                        <div id="Home_Details_be">
                            <span>Similar listings for rent</span>
                        </div>
                        <Row className="comman-margin-top">
                            <Col md={6} lg={4}>
                                <div id="kamuning-quezon_city">
                                    <div className="Rectangle_204">
                                        <div id="Rectangle_204">
                                            <img id="pexels-boonkong-boonpeng-11341" src={require('../images/pexels-boonkong-boonpeng-11341.png')} alt="pexel"></img>
                                            <div id="KamuningQuezon_City">
                                                <span>Kamuning+Quezon City</span>
                                            </div>
                                            <div id="ID728_N_Morgan_St_ChicagoIL606">
                                                <span>728 N Morgan St,<br />Chicago,IL,60640</span>
                                            </div>
                                            <div id="Studio-3_Bed__1564-10867">
                                                <span>Studio-3 Bed | $1,564-$10,867</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={6} lg={4}>
                            <div id="kamuning-quezon_city">
                                    <div className="Rectangle_204">
                                        <div id="Rectangle_204">
                                            <img id="pexels-boonkong-boonpeng-11341" src={require('../images/pexels-boonkong-boonpeng-11341.png')} alt="pexel"></img>
                                            <div id="KamuningQuezon_City">
                                                <span>Kamuning+Quezon City</span>
                                            </div>
                                            <div id="ID728_N_Morgan_St_ChicagoIL606">
                                                <span>728 N Morgan St,<br />Chicago,IL,60640</span>
                                            </div>
                                            <div id="Studio-3_Bed__1564-10867">
                                                <span>Studio-3 Bed | $1,564-$10,867</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={6} lg={4}>
                            <div id="kamuning-quezon_city">
                                    <div className="Rectangle_204">
                                        <div id="Rectangle_204">
                                            <img id="pexels-boonkong-boonpeng-11341" src={require('../images/pexels-boonkong-boonpeng-11341.png')} alt="pexel"></img>
                                            <div id="KamuningQuezon_City">
                                                <span>Kamuning+Quezon City</span>
                                            </div>
                                            <div id="ID728_N_Morgan_St_ChicagoIL606">
                                                <span>728 N Morgan St,<br />Chicago,IL,60640</span>
                                            </div>
                                            <div id="Studio-3_Bed__1564-10867">
                                                <span>Studio-3 Bed | $1,564-$10,867</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>

                </div>
            </Container>
        </section>
    );
}

export default PropertyDetails;
