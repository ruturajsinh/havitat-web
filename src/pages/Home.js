import React, { Component } from 'react'
import LandingPage from './LadingPage'
import Sell from './Sell'
import Rent from './Rent'
import PropertyDetails from './PropertyDetails'
import addProperty from './add_property'
import ManageMyProperty from './ManageMyProperty'
import CreateProperty from './CreateProperty'
import EditProperty from './EditProperty'
import LoginWithOtp from './LoginWithOtp'
import Page404 from './page404' // penign to add this router

import {
    BrowserRouter as Router,
    Route,
    NavLink
} from "react-router-dom";
import { Col, Container, Row } from 'react-bootstrap'

import Menu from '../components/Menu'
import MenuItem from '../components/MenuItem'
import Login from '../components/modal/Login'
import CreatePropertyModal from '../components/modal/CreateProperty'

import { Provider } from 'react-redux'
import jwt_decode from 'jwt-decode'
import store from '../store'
import setAuthToken from '../utils/setAuthToken'
import { setCurrentUser , logoutUser } from '../actions/authActions'
import PrivateRoute from '../PrivateRoute'

import Cookie from 'js-cookie'

if(Cookie.get('jwtToken')){
    const token = Cookie.get('jwtToken');

    localStorage.setItem("jwtToken",token);
}

if(localStorage.jwtToken){
    const token = localStorage.jwtToken;
    
    setAuthToken(token);

    const decoded = jwt_decode(token);
    localStorage.setItem("user_id",decoded.id);
    store.dispatch(setCurrentUser(decoded));

    const currentTime = Date.now() / 1000;
    if(decoded.exp < currentTime){
        store.dispatch(logoutUser());
        window.location.href = '/'
    }
}

class Home extends Component {

    constructor(props){
        super(props);
        
        this.state = {
            loginOpened: false,
            addPropertyOpened: false,
            menushow: false,
            name:"",
            email:"",
            password:"",
            confirm_password:"",
            number:"",
            errors:{},
            showLogin: false,
        };
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.errors){
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    onLogoutClick = e => {
        e.preventDefault();
        store.dispatch(logoutUser());
        
        const Timer = setTimeout(() => {
            window.location.href = '/'
        },2000);
    };

    showRight = () => {
        this.right.show();
        this.setState({menushow:true});
    };

    hideRight = () => {
        this.right.hide();
        this.setState({menushow:false});
    }


    render() {
        const AuthToken = localStorage.jwtToken ? true : false;
        return (
        <Provider store={store}>
            <div id="Havitat_web">
                <div id="havitat_web">
                    <Router history={Router}>
                        <div className="header">
                            <a href="/">
                                <img className="logo" src={require('../images/logo.svg')} alt="logo"></img>
                            </a>
                            <div className="Havitat">
                                <span>Havitat</span>
                            </div>

                            <div className="header-end">
                                <div id="sell">
                                    <NavLink to="/sell" activeClassName="excet-active"><span>Sell</span></NavLink >
                                </div>
                                <div id="Rent_d">
                                    <NavLink to="/rent" activeClassName="excet-active"><span>Rent</span></NavLink>
                                </div>
                                { AuthToken
                                ? <div className="Rectangle_183">
                                    <div className="dropdown">
                                        <button className="dropbtn">Profile</button>
                                        <div className="dropdown-content">
                                            <NavLink to="/create-property">Create Property</NavLink>
                                            <NavLink to="/manage-my-property">My Property</NavLink>
                                            <a href="#" onClick={this.onLogoutClick}>Logout</a>
                                        </div>
                                    </div>
                                  </div>
                                  : <div className="Rectangle_183">
                                        <NavLink to="/login"><button type="button" className="ease" name="signin" id="Rectangle_183">Sign in</button></NavLink>
                                    </div> }
                            </div>

                            <div className="responsive-menu">
                            {this.state.menushow 
                            ? <button className="responsive-menu-button" onClick={this.hideRight}>
                                <i className="fa fa-times"></i>
                            </button>
                            : <button className="responsive-menu-button" onClick={this.showRight}>
                                <i className="fa fa-bars"></i>
                            </button>
                            }
                                <Menu ref={right => this.right = right}>
                                <MenuItem hash=""><a href="/sell">Sell</a></MenuItem>
                                <MenuItem hash=""><a href="/rent">Rent</a></MenuItem>
                                <MenuItem hash=""><a href="#" onClick={this.onLoginClick}>Sign Up</a></MenuItem>
                                </Menu>
                            </div>
                            {/* area for create property modal */}
                            <CreatePropertyModal></CreatePropertyModal>
                        </div>

                        {/* <Switch> */}
                            <div className="App">
                                <Route exact path="/" component={LandingPage} />
                                <Route path="/sell" component={Sell} />
                                <Route path="/rent" component={Rent} />
                                <Route path="/login-with-otp" component={LoginWithOtp}></Route>
                                <Route path="/property-details" component={PropertyDetails} />
                                <PrivateRoute path="/add-property" component={addProperty} />
                                <PrivateRoute path="/manage-my-property" component={ManageMyProperty} />
                                <PrivateRoute path="/create-property" component={CreateProperty} />
                                <PrivateRoute path="/edit-property/:property_id" component={EditProperty}></PrivateRoute>
                                <Route exact path="/login" component={Login} />
                                {/* <Route component={Page404} /> */}
                            </div>
                        {/* </Switch> */}
                    </Router>

                    {/* start footer from here. */}
                    <div id="footer">
                        <Container fluid>
                            <Row>
                                <Col md={6} lg={3}>
                                    <div id="subscribe">
                                        <div id="Subscribe_dc">
                                            <span>Subscribe</span>
                                        </div>
                                        <form>
                                            <div id="your_email_address">
                                                <input type="email" name="email" id="Rectangle_29" placeholder="Your Email Address"></input>
                                            </div>
                                            <div id="subscribe_dg">
                                                <button type="submit" id="Rectangle_30" >Subscribe</button>
                                            </div>
                                        </form>
                                    </div>
                                </Col>

                                <Col md={6} lg={4}>
                                    <div id="havitat_markets">
                                        <div id="Havitat_Markets_c">
                                            <span>Havitat Markets</span>
                                        </div>
                                        <div id="___Houses_for_Rent_in_Makati_D">
                                            <ul className="list-unstyled">
                                                <a href="/rent"><li>Houses for Rent in Makati Dorms</li></a>
                                                <a href="/sell"><li>Houses for sales in Makati Dorms</li></a>
                                                <a href="/rent"><li>Houses for rent in BGC</li></a>
                                                <a href="/sell"><li>Houses for sale in BGC</li></a>
                                            </ul>
                                        </div>
                                    </div>
                                </Col>

                                <Col md={6} lg={3}>
                                    <div id="quick_links">
                                        <div id="Quick_links________">
                                            <span>Quick links</span>
                                        </div>
                                        <div id="About_us_Write_us_Terms__Condi">
                                            <ul className="list-unstyled footer-ul-list">
                                                <li>About us</li>
                                                <li>Write us</li>
                                                <li>Terms & Conditions</li>
                                                <li>Knowledgebase</li>
                                            </ul>
                                        </div>
                                    </div>
                                </Col>

                                <Col md={6} lg={2}>
                                    <div id="Became_a_Lessor_Seller_registr">
                                        <ul className="list-unstyled footer-ul-list footer-s4-linkes">
                                            <li >Became a Lessor</li>
                                            <li >Seller registration</li>
                                            <li >Renter registration</li>
                                            <li >Buyer registration</li>
                                        </ul>
                                    </div>
                                </Col>
                            </Row>
                        </Container>

                        <svg className="Line_5" viewBox="0 0 1600 1">
                            <path id="Line_5" d="M 0 0 L 1600 0">
                            </path>
                        </svg>

                        <Container fluid>
                            <Row>
                                <Col md={10}>
                                    <div id="Copyright_2020_Hevitet_Philipp">
                                        <span>Copyright @2020 Havitat Philippines All rights reserved</span>
                                    </div>
                                </Col>
                                <Col md={2}>
                                    <Row className="socialIcon">
                                        <Col>
                                        {/* <a href="fb://facewebmodal/f?href=https://it-it.facebook.com/fcbarcelona/"> */}
                                            <div id="facebook">
                                                <svg className="Ellipse_2">
                                                    <ellipse id="Ellipse_2" rx="12" ry="12" cx="12" cy="12">
                                                    </ellipse>
                                                </svg>
                                                <img id="facebook_cs" src={require('../images/facebook.svg')} alt="facebook" ></img>
                                            </div>
                                            {/* </a> */}
                                        </Col>
                                        <Col>
                                        {/* <a href="instagram://user?username={username}"> */}
                                            <div id="instagram">
                                                <svg className="Ellipse_2">
                                                    <ellipse id="Ellipse_2" rx="12.5" ry="12" cx="12.5" cy="12">
                                                    </ellipse>
                                                </svg>
                                                <img id="instagram-sketched" src={require('../images/instagram-sketched.svg')} alt="instagram"></img>
                                            </div>
                                        {/* </a> */}
                                        </Col>
                                        <Col>
                                        {/* <a href="twitter://user?user_id=2"> */}
                                            <div id="twitter">
                                                <svg className="Ellipse_2">
                                                    <ellipse id="Ellipse_2" rx="12.10138988494873" ry="12.293475151062012" cx="12.10138988494873" cy="12.293475151062012">
                                                    </ellipse>
                                                </svg>
                                                <img id="twitter_cy" src={require('../images/twitter.svg')} alt="twitter"></img>
                                            </div>
                                        {/* </a> */}
                                        </Col>
                                        <Col>
                                            <div id="youtube">
                                                <svg className="Ellipse_2">
                                                    <ellipse id="Ellipse_2" rx="12" ry="12" cx="12" cy="12">
                                                    </ellipse>
                                                </svg>
                                                <img id="youtube_c" src={require('../images/youtube.svg')} alt="youtube"></img>
                                            </div></Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </div>
            </div>
        </Provider>

        );
    }
}

export default Home;