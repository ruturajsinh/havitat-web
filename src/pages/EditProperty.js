import React, { Component } from 'react'
import { Col, Container, Row } from 'react-bootstrap';

import ResidentalWLand from '../components/property_component/ResidentalWLand'
import ResidentalWOLand from '../components/property_component/ResidentalWOLand'
import Office from '../components/property_component/Office'
import Commercial from '../components/property_component/Commercial'
import Land from '../components/property_component/Land'
import Storage from '../components/property_component/Storage'
import Leisure from '../components/property_component/Leisure'
import Business from '../components/property_component/Business'
import Memberships from '../components/property_component/Membership'
import Advertising from '../components/property_component/Advertising'
import Loading from '../components/Loading'
import WarningModal from '../components/modal/WarningModal'
import SuccessModal from '../components/modal/SuccessModal'

import axios from 'axios';

class EditProperty extends Component{
    constructor(props){
        super(props);

        this.state = {
            property_type:'Residential w/Land',
            fieldMap:{
                "Residential w/Land":[
                    { value:"House", label:"House" },
                    { value:"Villa", label:"Villa" },
                    { value:"Bunglow", label:"Bunglow" }
                ],
                "Residential w/o Land":[
                    { value:"Condo", label:"Condo" },
                    { value:"Apartment", label: "Apartment" },
                    { value:"Dormitory", label:"Dormitory" },
                    { value:"Room", label:"Room" },
                    { value:"Bed", label:"Bed" }
                ],
                "Office":[
                    { value:"Office Unit", label:"Office Unit" },
                    { value:"Office Floor", label:"Office Floor" },
                    { value:"Office Building", label:"Office Building" }
                ],
                "Commercial":[
                    { value:"Shop", label:"Shop" },
                    { value:"Restaurant/Cafe", label:"Restaurant/Cafe" },
                    { value:"Hotel/Hostel", label:"Hotel/Hostel" }
                ],
                "Land":[
                    { value:"Residential Land", label:"Residential Land" },
                    { value:"Commercial Land", label:"Commercial Land" },
                    { value:"Farm Land", label:"Farm Land" },
                    { value:"Forest Land", label:"Forest Land" },
                    { value:"Aqualculture", label:"Aqualculture" },
                    { value:"Island", label:"Island" }
                ],
                "Storage":[
                    { value:"Warehouse", label:"Warehouse" },
                    { value:"Storage Space", label:"Storage Space" },
                    { value:"Locker", label:"Locker" },
                    { value:"Garage", label:"Garage" },
                    { value:"Parking", label:"Parking" }
                ],
                "Agriculture":[
                    { value:"Fruit Farm", label:"Fruit Farm" },
                    { value:"Vegetable Farm", label:"Vegetable Farm" },
                    { value:"Flower Farm", label:"Flower Farm" },
                    { value:"Piggery", label:"Piggery" },
                    { value:"Animal Farm", label:"Animal Farm" }
                ],
                "Leisure":[
                    { value:"Holiday Property", label:"Holiday Property" },
                    { value:"Time Share", label:"Time Share" },
                    { value:"Hobby Farm", label:"Hobby Farm" },
                    { value:"Leisure Condo", label:"Leisure Condo" },
                    { value:"Holiday Villa", label:"Holiday Villa" },
                    { value:"Retirement Home", label:"Retirement Home" }
                ],
                "Business":[
                    { value:"Franchise", label:"Franchise" },
                    { value:"Shop", label:"Shop" },
                    { value:"Restaurant", label:"Restaurant" },
                    { value:"Cafe", label:"Cafe" },
                    { value:"Hotel/Hostel", label:"Hotel/Hostel" },
                    { value:"Food Cart", label:"Food Cart" }
                ],
                "Memberships":[
                    { value:"Leisure Club", label:"Leisure Club" },
                    { value:"Golf Club", label:"Golf Club" },
                    { value:"Holiday Club", label:"Holiday Club" },
                    { value:"Sport Club", label:"Sport Club" }
                ],
                "Advertising":[
                    { value:"Billboard", label:"Billboard" },
                    { value:"Electronic Billboard", label:"Electronic Billboard" },
                    { value:"Billboard Space", label:"Billboard Space" },
                    { value:"Wall Signage", label:"Wall Signage" }
                ]
            },
            fields:{},
            errors:{},
            offer_price:{},
            location_info:{},
            neighborhood_info:{},
            property_images: null,

        }
    }

    componentWillMount(){
        this.getPropertyDetails();
    }

    getPropertyDetails(){
        
        let property_id = this.props.match.params.property_id;
        
        axios.get('/get-property-details/' + property_id).then(res => {
            this.setState({ 
                fields: res.data.data,
                offer_price:res.data.data.offer_price,
                neighborhood_info: res.data.data.neighborhood_info
             });
             this.childComRef.setState(res.data.data.property_details);
            console.log();
        }).catch(err => {console.log(err);});
    }

    getOptions = (key) => {
        if(!this.state.fieldMap[key]){
            return null;
        }

        return this.state.fieldMap[key].map(function(el,i){
            return <option key={i} value={el.value} >{el.label}</option>;
        });
    }

    handlePropertyTypeChange(e){
        this.setState({
            property_type: e.target.value
        });
    };

    getOptions(key){
        if(!this.state.property_type[key]){
            return null;
        }
        return this.state.property_type[key].map(function(el,i){
            return <option key={i} value={el.value}>{el.label}</option>;
        });
    }

    getDefaultSub(){
        let key = "Residential w/Land";
        return this.state.fieldMap[key].map(function(el,i){
            return <option key={i} value={el.value}>{el.label}</option>;
        });
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    getSubProperty = () => {
        if(!this.state.property_type){
            return this.getDefaultSub();
        }
        return this.getOptions(this.state.property_type);
    }


    getDefaultStep2(){
        return <ResidentalWLand ref={childComRef => this.childComRef = childComRef} />;
    }


    getStep2DynamicSection(){
        if(this.state.property_type === "Residential w/Land"){
            return this.getDefaultStep2();
        }else if(this.state.property_type === "Residential w/o Land"){
            return <ResidentalWOLand ref={childComRef => this.childComRef = childComRef} />;
        }else if (this.state.property_type === "Office"){
            return <Office ref={childComRef => this.childComRef = childComRef}/>
        }else if (this.state.property_type === "Commercial"){
            return <Commercial ref={childComRef => this.childComRef = childComRef} />
        }else if (this.state.property_type === "Land"){
            return <Land ref={childComRef => this.childComRef = childComRef} />
        }else if (this.state.property_type === "Storage"){
            return <Storage ref={childComRef => this.childComRef = childComRef}/>
        }else if(this.state.property_type === "Agriculture"){
            
        }else if(this.state.property_type === "Leisure"){
            return <Leisure ref={childComRef => this.childComRef = childComRef}/>
        }else if(this.state.property_type === "Business"){
            return <Business ref={childComRef => this.childComRef = childComRef}/>
        }else if(this.state.property_type === "Memberships"){
            return <Memberships ref={childComRef => this.childComRef = childComRef}/>
        }else if(this.state.property_type === "Advertising"){
            return <Advertising ref={childComRef => this.childComRef = childComRef}/>
        }

        return this.getDefaultStep2();
    }

    handleChange(field,e){
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({fields});
    }

    handleValidation(){
        let fields = this.state.fields
        let errors = {};
        let formIsValid = true;

        if(!fields["property_title"]){
            formIsValid = false;
            errors.property_title = "Property Title can not be empty";
        }

        if(!fields["property_address"]){
            formIsValid = false;
            errors.property_address = "Property Address can not be empty"
        }

        if(!fields["property_transaction_category"]){
            formIsValid = false;
            errors.property_transaction_category = "Please select Property Transaction Category"
        }
        
        if(this.state.property_images === null){
            formIsValid = false;
            errors.property_images = "At least select one image for create property";
        }
        

        this.setState({ errors: errors });
        return formIsValid;
    }

    onFileChange(e){
        this.setState({ property_images: e.target.files });
    }


    onUpdateProperty = e => {
        e.preventDefault();
        this.loading.show();
        
        if(this.handleValidation()){

            const update_property = new FormData();
            update_property.append("user_id",localStorage.getItem("user_id"));
            update_property.append("property_id", this.props.match.params.property_id);
            update_property.append("property_title",this.state.fields["property_title"]);
            update_property.append("property_address",this.state.fields["property_address"]);
            update_property.append("property_transaction_category",this.state.fields["property_transaction_category"]);
            update_property.append("property_type",this.state.property_type);
            update_property.append("property_sub_type",this.state.fields["property_sub_type"]);
        
        for(const key of Object.keys(this.state.property_images)){
            update_property.append("property_images",this.state.property_images[key]);
        }

        let property_details = {
            lot_area: this.childComRef.state.lot_area,
            floor_area: this.childComRef.state.floor_area,
            rooms: this.childComRef.state.rooms,
            bedrooms: this.childComRef.state.bedrooms,
            bathrooms: this.childComRef.state.bathrooms,
            kitchen: this.childComRef.state.kitchen,
            home_gym: this.childComRef.state.home_gym,
            home_cinema: this.childComRef.state.home_cinema,
            home_bar: this.childComRef.state.home_bar,
            den: this.childComRef.state.den,
            pool: this.childComRef.state.pool,
            pool_size: this.childComRef.pool_size,
            maids_rooms: this.childComRef.maids_rooms,
            number_of_mads_bed: this.childComRef.state.number_of_maids_beds,
            driver_rooms: this.childComRef.state.driver_rooms,
            number_of_driver_bed: this.childComRef.state.number_of_driver_beds,
            balcony: this.childComRef.state.balcony,
            terrace: this.childComRef.state.terrace,
            lanai: this.childComRef.state.lanai,
            garden: this.childComRef.state.garden,
            garden_size: this.childComRef.state.garden_size,
            garage: this.childComRef.state.garage,
            parking: this.childComRef.state.parking,
            parking_slots: this.childComRef.state.parking_slots,
            status: this.childComRef.state.status,
            furnishing: this.childComRef.state.furnishing,
            association_dues: this.childComRef.state.association_dues
        }
        
        let property_details_2 = {
            floor_area: this.childComRef.state.floor_area,
            rooms: this.childComRef.state.rooms,
            bedrooms: this.childComRef.state.bedrooms,
            bathrooms: this.childComRef.state.bathrooms,
            kitchen: this.childComRef.state.kitchen,
            home_office: this.childComRef.state.home_office,
            home_gym: this.childComRef.state.home_gym,
            home_cinema: this.childComRef.state.home_cinema,
            home_bar: this.childComRef.state.home_bar,
            den: this.childComRef.state.den,
            maids_rooms: this.childComRef.state.madis_room,
            number_of_maids_rooms: this.childComRef.state.number_of_maids_rooms,
            balcony: this.childComRef.state.balcony,
            terrace: this.childComRef.state.terrace,
            parking: this.childComRef.state.parking,
            parking_slots: this.childComRef.state.parking_slots,
            status: this.childComRef.state.status,
            furnishing: this.childComRef.state.furnishing,
            association_dues: this.childComRef.state.association_dues,
            amenities:{
                pool: this.childComRef.state.pool,
                gym: this.childComRef.state.gym,
                play_room: this.childComRef.state.play_room,
                function_room: this.childComRef.state.function_room,
                reception_room: this.childComRef.state.reception_room,
                commuunal_desk: this.childComRef.state.commuunal_desk,
                retail_area: this.childComRef.state.retail_area
            }
        }

        let property_details_3 = {
            floor_area: this.childComRef.state.floor_area,
            office_area: this.childComRef.state.office_area,
            desks_seats: this.childComRef.state.desks_seats,
            status: this.childComRef.state.status,
            furnishing: this.childComRef.state.furnishing,
            association_dues: this.childComRef.state.association_dues,
            amenities:{
                pool: this.childComRef.state.pool,
                gym: this.childComRef.state.gym,
                play_room: this.childComRef.state.play_room,
                function_room: this.childComRef.state.function_room,
                reception: this.childComRef.state.reception,
                commuunal_desk: this.childComRef.state.commuunal_desk,
                retail_area: this.childComRef.state.retail_area
            }
        }
        let property_details_4 = {
            commercial_type: this.childComRef.state.commercial_type,
            total_area: this.childComRef.state.total_area,
            commercial_area: this.childComRef.state.commercial_area,
            storage_area: this.childComRef.state.storage_area,
            office_area: this.childComRef.state.office_area,
            type: this.childComRef.state.type
        }
        let property_details_5 = {
            land_type: this.childComRef.state.land_type,
            land_area: this.childComRef.state.land_area,
            land_description: this.childComRef.state.land_description,
            ownership: this.childComRef.state.ownerships,
            land_designation: this.childComRef.state.land_designation
        }
        let property_details_6 = {
            storage_type: this.childComRef.state.storage_type,
            storage_area: this.childComRef.state.storage_area,
            storage_description: this.childComRef.state.storage_description
        }
        let property_details_7 = {

        }
        let property_details_8 = {
            leisure_type: this.childComRef.state.leisure_type,
            leisure_description: this.childComRef.state.leisure_description
        }
        let property_details_9 = {
            business_type: this.childComRef.state.business_type,
            business_description: this.childComRef.state.business_description,
            turnover_per_annum: this.childComRef.state.turnover_per_annum
        }
        let property_details_10 = {
            memebership_type: this.childComRef.state.membership_type,
            memebership_description: this.childComRef.state.memebership_description,
            memebership_rights: this.childComRef.state.memebership_rights,
            membership_fees: this.childComRef.state.memebership_rights
        }
        let property_details_11 = {
            advertising_type: this.childComRef.state.advertising_type,
            display_area: this.childComRef.state.display_area,
            display_description: this.childComRef.state.display_description,
            eyes_per_day: this.childComRef.state.eyes_per_day
        }

        if(this.state.property_type === 'Residential w/Land'){
            update_property.append("property_details",JSON.stringify(property_details));
        }else if(this.state.property_type === 'Residential w/o Land'){
            update_property.append("property_details",JSON.stringify(property_details_2))
        }else if(this.state.property_type === 'Office'){
            update_property.append("property_details",JSON.stringify(property_details_3))
        }else if(this.state.property_type === 'Commercial'){
            update_property.append("property_details",JSON.stringify(property_details_4))
        }else if(this.state.property_type === 'Land'){
            update_property.append("property_details",JSON.stringify(property_details_5))
        }else if(this.state.property_type === 'Storage'){
            update_property.append("property_details",JSON.stringify(property_details_6))
        }else if(this.state.property_type === 'Agriculture'){
            update_property.append("property_details",JSON.stringify(property_details_7))
        }else if(this.state.property_type === "Leisure"){
            update_property.append("property_details",JSON.stringify(property_details_8))
        }else if(this.state.property_type === "Business"){
            update_property.append("property_details",JSON.stringify(property_details_9))
        }else if(this.state.property_type === "Memberships"){
            update_property.append("property_details",JSON.stringify(property_details_10))
        }else if(this.state.property_type === "Advertising"){
            update_property.append("property_details",JSON.stringify(property_details_11))
        }

        let location_info = {
            street: this.state.location_info.street,
            number: this.state.location_info.number,
            barangray: this.state.location_info.barangray,
            city: this.state.location_info.city,
            country: this.state.location_info.country
        }
        update_property.append("location_info",JSON.stringify(location_info));
        let neighborhood_info = {
            nearest_school: this.state.neighborhood_info.nearest_school,
            neighborhood_description: this.state.neighborhood_info.neighborhood_description,
            nearest_public_transportation: this.state.neighborhood_info.nearest_public_transportation,
            nearest_commercial_area: this.state.neighborhood_info.nearest_commercial_area,
            nearest_major_road: this.state.neighborhood_info.nearest_major_road
        }

        update_property.append("neighborhood_info",JSON.stringify(neighborhood_info));
        let offer_price = {
            offer_price: this.state.offer_price.offer_price,
            transfer_expenses: this.state.offer_price.transfer_expenses,
            transaction_expenses: this.state.offer_price.transaction_expenses,
            broker_fees: this.state.offer_price.broker_fees,
            taxes: this.state.offer_price.taxes,
            fees: this.state.offer_price.fees
        }
        update_property.append("offer_price",JSON.stringify(offer_price));

            axios({
                method:"POST",
                url:'/update-property',
                data: update_property,
                headers:{'Content-Type': 'multipart/form-data'}
            }).then(res =>{
                
                if(res.data.status == 'success'){
                    this.success.handleShow();
                    this.success.successText('congratulations, Your property updated successfully');
                    this.success.setRedirectUrl('/manage-my-property');
                }else{
                    this.success.handleShow();
                    this.success.successText('Something went while updating property, Please try again or later.');
                    this.success.setRedirectUrl('/manage-my-property');
                }

                this.loading.hide();
            }).catch(err => {
                console.log(err);
                this.loading.hide();
            })
        }else{
            this.show.handleShow();
            this.show.warningText("There is must be an error please check before create property.");
            this.loading.hide();
        }
    }


    render(){
        return(
            <div id="listing_information">
                <Loading ref={loading => this.loading = loading}></Loading>
                <WarningModal ref={show => this.show = show}></WarningModal>
                <SuccessModal ref={success => this.success = success} ></SuccessModal>
                <div id="LISTING_INFORMATION_u">
                    <span>Edit {this.state.fields["property_title"]} Information</span>
                </div>
                <Container>
                    <Row>
                    <div className="left-side-title">
                        <h4>Step : 1</h4>
                    </div>
                    <div className="pt-2"></div>
                    </Row>
                    <Row>
                    <Col md={4}>
                            <div>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="property-title" className="extra-input-height-42" type="text" name="property-title" value={this.state.fields["property_title"]} onChange={this.handleChange.bind(this,"property_title")} required />
                                    <label htmlFor="property-title" >Property Title</label>
                                    <span className="red-text">{this.state.errors.property_title}</span>
                                </div>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="property-address" type="text" className="extra-input-height-42" name="property-address" value={this.state.fields["property_address"]} onChange={this.handleChange.bind(this,"property_address")} required/>
                                    <label htmlFor="property-address">Property Address</label>
                                    <span className="red-text">{this.state.errors.property_address}</span>
                                </div>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="comman-input-area input-field">
                                <select id="property-type" className="command-dropdown-with-border" value={this.state.fields["property_transaction_category"]} onChange={this.handleChange.bind(this,"property_transaction_category")} >
                                    <option value="">Property Transaction Category</option>
                                    <option value="For Sale">For Sale</option>
                                    <option value="For Rent">For Rent</option>
                                    <option value="For Joint Venture">For Joint Venture</option>
                                </select>
                                <span className="red-text">{this.state.errors.property_transaction_category}</span>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="left-side-title">
                                <h6>Property Types : </h6>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="comman-input-area input-field">
                                <select id="property-type" className="command-dropdown-with-border" onChange={(e) => this.handlePropertyTypeChange(e)} value={this.state.property_type}>
                                    <option>Select Property Type</option>
                                    <option value="Residential w/Land">Residential w/Land</option>
                                    <option value="Residential w/o Land">Residential w/o Land</option>
                                    <option value="Office">Office</option>
                                    <option value="Commercial">Commercial</option>
                                    <option value="Land">Land</option>
                                    <option value="Storage">Storage</option>
                                    <option value="Agriculture">Agriculture</option>
                                    <option value="Leisure">Leisure</option>
                                    <option value="Business">Business</option>
                                    <option value="Memberships">Memberships</option>
                                    <option value="Advertising">Advertising</option>
                                </select>
                                <span className="red-text">{this.state.errors.property_type}</span>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="comman-input-area input-field">
                                <select id="property-type" className="command-dropdown-with-border" value={this.state.fields["property_sub_type"]} onChange={this.handleChange.bind(this,"property_sub_type")}>
                                    <option>Select Sub Property</option>
                                    {this.getSubProperty()}
                                </select>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="comman-input-area input-field">
                                <input type="file" className="form-control select-pro-image" name="property_images" onChange={ (e) => this.onFileChange(e)} multiple></input>
                                <span className="red-text">{this.state.errors.property_images}</span>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <div className="left-side-title">
                            <h4>Step : 2</h4>
                        </div>
                        <div className="mt-5"></div>
                    </Row>

                    {/* here is dynamice step 2 start */}
                        {this.getStep2DynamicSection()}
                    {/* dynamice step 2 end */}
                    
                    <div className="step-three-property">
                        <Row>
                            <div className="left-side-title">
                                <h4>Step : 3</h4>
                            </div>

                            <Col md={12}>
                                <div className="left-side-title">
                                    <div className="inner-title">
                                        <h6>Location Information : </h6>
                                    </div>
                                </div>
                            </Col>

                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="street" type="text" className="extra-input-height-42" name="street" value={this.state.location_info.street} onChange={this.handleChange.bind(this,"street")} required/>
                                    <label htmlFor="street">Street </label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="number" type="tel" className="extra-input-height-42" name="number" maxLength="12" value={this.state.location_info.number} onChange={this.handleChange.bind(this,"number")} required/>
                                    <label htmlFor="number">Number</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="barangray" type="text" className="extra-input-height-42" name="barangray"  value={this.state.location_info.barangray} onChange={this.handleChange.bind(this,"barangray")} required/>
                                    <label htmlFor="barangray" >Barangray</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="city" type="text" className="extra-input-height-42" name="city" value={this.state.location_info.city} onChange={this.handleChange.bind(this,"city")} required/>
                                    <label htmlFor="city" >City</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="country" type="text" className="extra-input-height-42" name="country" value={this.state.location_info.country} onChange={this.handleChange.bind(this,"country")} required/>
                                    <label htmlFor="country" >Country</label>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={12}>
                                <div className="left-side-title">
                                    <div className="inner-title">
                                        <h6>Neighborhood Information : </h6>
                                    </div>
                                </div>
                            </Col>

                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="neighborhood_description" type="text" className="extra-input-height-42" name="neighborhood_description" value={this.state.neighborhood_info.neighborhood_description} onChange={this.handleChange.bind(this,"neighborhood_description")} required/>
                                    <label htmlFor="neighborhood_description" >Neighborhood Description</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="nearest_school" type="text" className="extra-input-height-42" name="nearest_school" value={this.state.neighborhood_info.nearest_school} onChange={this.handleChange.bind(this,"nearest_school")} required/>
                                    <label htmlFor="nearest_school" >Nearest School</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="nearest_public_transportation" type="text" className="extra-input-height-42" name="nearest_public_transportation" value={this.state.neighborhood_info.nearest_public_transportation} onChange={this.handleChange.bind(this, "nearest_public_transportation")} required/>
                                    <label htmlFor="nearest_public_transportation" >Nearest Public Transportation</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="nearest_commercial_area" type="text" className="extra-input-height-42" name="nearest_commercial_area" value={this.state.neighborhood_info.nearest_commercial_area} onChange={this.handleChange.bind(this,"nearest_commercial_area")} required/>
                                    <label htmlFor="nearest_commercial_area" >Nearest Commercial Area</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="nearest_major_road" type="text" className="extra-input-height-42" name="nearest_major_road" value={this.state.neighborhood_info.nearest_major_road} onChange={this.handleChange.bind(this,"nearest_major_road")} required/>
                                    <label htmlFor="nearest_major_road" >Nearest Major Road</label>
                                </div>
                            </Col>
                        </Row>

                        <Row>
                            <Col md={12}>
                                <div className="left-side-title">
                                    <div className="inner-title">
                                        <h6>Offer Price : </h6>
                                    </div>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="offer_price" type="text" className="extra-input-height-42" name="offer_price" value={this.state.offer_price.offer_price} onChange={this.handleChange.bind(this,"offer_price")} required/>
                                    <label htmlFor="offer_price" >Offer Price</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="transfer_expenses" type="text" className="extra-input-height-42" name="transfer_expenses" value={this.state.offer_price.transfer_expenses} onChange={this.handleChange.bind(this,"transfer_expenses")} required/>
                                    <label htmlFor="transfer_expenses" >Transfer Expenses</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="transaction_expenses" type="text" className="extra-input-height-42" name="transaction_expenses" value={this.state.offer_price.transaction_expenses} onChange={this.handleChange.bind(this,"transaction_expenses")} required/>
                                    <label htmlFor="transaction_expenses" >Transaction Expenses</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="broker_fees" type="text" className="extra-input-height-42" name="broker_fees" value={this.state.offer_price.broker_fees} onChange={this.handleChange.bind(this,"broker_fees")} required/>
                                    <label htmlFor="broker_fees" >Broker Fees</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="taxes" type="text" className="extra-input-height-42" name="taxes" value={this.state.offer_price.taxes} onChange={this.handleChange.bind(this,"taxes")} required/>
                                    <label htmlFor="taxes" >Taxes</label>
                                </div>
                            </Col>
                            <Col md={3}>
                                <div className="comman-input-area-with-border input-field">
                                    <input id="fees" type="text" className="extra-input-height-42" name="fees" value={this.state.offer_price.fees} onChange={this.handleChange.bind(this,"fees")} required/>
                                    <label htmlFor="fees" >Fees</label>
                                </div>
                            </Col>
                        </Row>

                    </div>
                    <Row>
                        <button className="comman-theme-btn" onClick={this.onUpdateProperty}>Update</button>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default EditProperty;