import React from 'react'
import { Col, Row, Container } from 'react-bootstrap';

function Footer() {
    return (
        <div id="footer">
            <svg className="background_cp" viewBox="0 0 1600 658">
                <path id="background_cp" d="M 0 0 L 1600 0 L 1600 658.0000610351563 L 0 658.0000610351563 L 0 0 Z">
                </path>
            </svg>
            <Container fluid>
                <Row>
                    <Col xs={3}>
                        <div id="subscribe">
                            <div id="Subscribe_dc">
                                <span>Subscribe</span>
                            </div>
                            <div id="your_email_address">
                                <input type="email" name="email" id="Rectangle_29" placeholder="Your Email Address"></input>
                            </div>
                            <div id="subscribe_dg">
                                <button type="button" id="Rectangle_30" >Subscribe</button>
                            </div>
                        </div>
                    </Col>

                    <Col xs={4}>
                        <div id="havitat_markets">
                            <div id="Havitat_Markets_c">
                                <span>Havitat Markets</span>
                            </div>
                            <div id="___Houses_for_Rent_in_Makati_D">                                
                            <ul className="list-unstyled">
                                <li>Houses for Rent in Makati Dorms</li>
                                <li>Houses for sales in Makati Dorms</li>
                                <li>Houses for rent in BGC</li>
                                <li>Houses for sale in BGC</li>
                            </ul>
                            </div>
                        </div>
                    </Col>

                    <Col xs={3}>
                        <div id="quick_links">
                        <div id="Quick_links________">
                                <span>Quick links</span>
                            </div>
                            <div id="About_us_Write_us_Terms__Condi">
                                <ul className="list-unstyled">
                                    <li>About us</li>
                                    <li>Write us</li>
                                    <li>Terms & Condition</li>
                                    <li>Knowledgebase</li>
                                </ul>
                            </div>
                        </div>
                    </Col>

                    <Col xs={2}>
                        <div id="Became_a_Lessor_Seller_registr">
                            <ul className="list-unstyled">
                                <li>Became a Lessor</li>
                                <li>Seller registration</li>
                                <li>Renter registration</li>
                                <li>Buyer registration</li>
                            </ul>
                        </div>
                    </Col>
                </Row>
            </Container>

            <svg className="Line_5" viewBox="0 0 1600 1">
                <path id="Line_5" d="M 0 0 L 1600 0">
                </path>
            </svg>

            <Container fluid>
                <Row>
                    <Col xs={10}>
                        <div id="Copyright_2020_Hevitet_Philipp">
                            <span>Copyright @2020 Havitat Philippines All rights reserved</span>
                        </div>
                    </Col>
                    <Col xs={2}>
                        <Row className="socialIcon">
                            <Col>
                            <div id="facebook">
                            <svg className="Ellipse_2">
                                <ellipse id="Ellipse_2" rx="12" ry="12" cx="12" cy="12">
                                </ellipse>
                                </svg>
                                <img id="facebook_cs" src={require('../images/facebook.svg')} ></img>
                            </div>
                            </Col>
                            <Col>
                            <div id="instagram">
                            <svg className="Ellipse_3">
                                <ellipse id="Ellipse_3" rx="12.5" ry="12" cx="12.5" cy="12">
                                </ellipse>
                            </svg>
                            <img id="instagram-sketched" src={require('../images/instagram-sketched.svg')}></img>
                        </div>
                            </Col>
                            <Col>
                            <div id="twitter">
                            <svg className="Ellipse_1">
                                <ellipse id="Ellipse_1" rx="12.10138988494873" ry="12.293475151062012" cx="12.10138988494873" cy="12.293475151062012">
                                </ellipse>
                            </svg>
                            <img id="twitter_cy" src={require('../images/twitter.svg')}></img>
                        </div>
                            </Col>
                            <Col>
                            <div id="youtube">
                            <svg className="Ellipse_6">
                                <ellipse id="Ellipse_6" rx="12" ry="12" cx="12" cy="12">
                                </ellipse>
                            </svg>
                            <img id="youtube_c" src={require('../images/youtube.svg')}></img>
                        </div></Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
    
        </div>
    );
}

export default Footer;