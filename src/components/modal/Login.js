import React from "react";
import { Col, Modal, Row, Tab, Tabs } from 'react-bootstrap'
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser, registerUser } from "../../actions/authActions";
import classnames from "classnames";
import Loading from '../Loading'
import config from '../../config'

export class Login extends React.Component {
    constructor(props,context) {
        super(props,context);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            name:'',
            email: "",
            password: "",
            c_password:'',
            number:'',
            errors: {},
            show: true,
            loading: false,
            base_url: config.backend_base_url
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.isAuthenticated) {
            this.loading.show();
            window.location.href = "/create-property"; // push user to create property Page when they login
        }
        if (nextProps.errors) {
            console.log(nextProps.errors);
            this.setState({
                errors: nextProps.errors
            });
        }
    }

    handleShow(){
        this.setState({ show: true });
    }

    handleClose(){
        this.setState({ show: false });
        window.location.href = "/";
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
    onSubmit = e => {
        e.preventDefault();
        const userData = {
            email: this.state.email,
            password: this.state.password
        };
        
        this.props.loginUser(userData); // since we handle the redirect within our component, we don't need to pass in this.props.history as a parameter
    };

    onSignup = e => {
        e.preventDefault();
        
        const userData = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            c_password: this.state.c_password,
            number: this.state.number,
        }
        this.props.registerUser(userData, this.props.history);
    }

    render() {
        const { errors } = this.state;
        return (
            <div style={{"minHeight":'600px'}}>
                <Modal
                show={this.state.show}
                onHide={this.handleClose}
                backdrop="static"
                keyboard={false}
            >
            
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                <Loading ref={ loading => this.loading = loading }></Loading>
                    <Tabs defaultActiveKey="home" id="uncontrolled-tab-example">
                        <Tab eventKey="home" title="Sign In">
                            <div className="facebook-login-section">
                                <a href={ this.state.base_url + "auth/facebook" }>
                                <div className="facebook-login-box">
                                    <div className="social-icon-with-text">
                                        <img src={require('../../images/icons/facebook.svg')} alt="facebook"></img>
                                        <span className="social-login-text">Sign in with Facebook</span>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div className="sign-with-google-section">
                                <a href={this.state.base_url + "auth/google"}>
                                <div className="sign-with-google-box">
                                    <div className="social-icon-with-text">
                                        <img src={require('../../images/icons/google.svg')} alt="google"></img>
                                        <span className="social-login-black-text">Sign in with Google</span>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div className="sign-with-google-section">
                                <a href="/login-with-otp">
                                <div className="sign-with-google-box">
                                    <div className="social-icon-with-text">
                                        <img src={require('../../images/icons/mobile.svg')} alt="mobile"></img>
                                        <span className="social-login-black-text">Sign in with Mobile</span>
                                    </div>
                                </div>
                                </a>
                            </div>

                            <div className="underlineText">
                                <span>Or</span>
                            </div>
                            <form noValidate onSubmit={this.onSubmit}>
                                <div className="login-form">
                                    <div className="comman-input-area input-field">
                                        <input id="uniq_email" type="email" name="uniq_email" value={this.state.email} onChange={ (e) => this.setState({ email: e.target.value }) } autoComplete="new-password" required className={classnames("", { invalid: errors.email })} />
                                        <label htmlFor="uniq_email" >Enter email</label>
                                    </div>
                                    <span className="red-text"> {errors.email} {errors.emailnotfound}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="password" type="password" name="password" value={this.state.password} onChange={ (e) => this.setState({ password: e.target.value }) } autoComplete="new-password" required className={classnames("", { invalid: errors.password })} />
                                        <label htmlFor="password" >Enter password</label>
                                    </div>
                                    <span className="red-text"> {errors.password} {errors.passwordincorrect} </span>
                                    <Row>
                                        <div className="remember-me-area">
                                            <Col md={6}>
                                                <label className="checkbox-small-container">Remember
                                <input type="checkbox"></input>
                                                    <span className="checkmark-small"></span>
                                                </label>
                                            </Col>
                                            <Col md={6}>
                                                <div className="float-right">
                                                    <a href="/" className="orange-link-text">Forgot Password?</a>
                                                </div>
                                            </Col>
                                        </div>
                                    </Row>
                                    <div className="login-btn-area">
                                        <button className="form-submit-btn" type="submit">Login</button>
                                    </div>
                                </div>
                            </form>

                        </Tab>
                        <Tab eventKey="profile" title="Register">
                            <div className="modal-container">
                                <div className="login-form comman-margin-top">
                                    <form noValidate onSubmit={this.onSignup}>
                                    <div className="comman-input-area input-field">
                                        <input type="text" id="uniqe-name" value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }); }} required />
                                        <label htmlFor="uniqe-name">Name</label>
                                    </div>
                                    <span className="red-text"> {errors.name}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="uniqe-email" type="email" name="email" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }); }} required />
                                        <label htmlFor="uniqe-email" >Enter Email</label>
                                    </div>
                                    <span className="red-text"> {errors.email}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="uniqe-password" type="password" name="password" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }); }} required />
                                        <label htmlFor="uniqe-password" >Password</label>
                                    </div>
                                    <span className="red-text"> {errors.password}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="confirm_password" type="password" name="confirm_password" value={this.state.c_password} onChange={(e) => { this.setState({ c_password: e.target.value }) }} required />
                                        <label htmlFor="confirm_password" >Confirm Password</label>
                                    </div>
                                    <span className="red-text"> {errors.c_password}</span>
                                    <div className="comman-input-area input-field">
                                        <input id="mobile_number" type="tel" name="mobile_number" value={this.state.number} onChange={(e) => { this.setState({ number: e.target.value }) }} required />
                                        <label htmlFor="mobile_number" >Mobile Number</label>
                                    </div>
                                    <div className="login-btn-area">
                                        <button type="submit" className="form-submit-btn">Register</button>
                                    </div>
                                    <div className="terms-of-use-text">
                                        <span>By Submitting, I accept Havitat <span className="orange-link-text-underline">Terms Of Use</span></span>
                                    </div>
                                    </form>
                                </div>
                                <hr></hr>
                                <div className="other-option">
                                    <p>Or connect with :</p>
                                    <Row>
                                        <Col md={8} xs={8}>
                                            <img src={require('../../images/icons/google.svg')} alt="google"></img>
                                            <span className="pl-2">Sign in with Google</span>
                                        </Col>
                                        <Col md={2} xs={2}>
                                            <div className="social-icon-box">
                                                <img src={require('../../images/icons/facebook.svg')} alt="facebook"></img>
                                            </div>
                                        </Col>
                                        <Col md={2} xs={1}>
                                            <div className="social-icon-box">
                                                <img src={require('../../images/icons/mobile.svg')} alt="mobile"></img>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </Tab>
                    </Tabs>
                </Modal.Body>
            </Modal>
            </div>
        );
    }
}
Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
});
export default connect(
    mapStateToProps,
    { loginUser,registerUser }
)(Login);