import React from 'react'
import { Modal } from 'react-bootstrap';

export default class WarningModal extends React.Component{
    constructor(props){
        super(props);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    
        this.state = {
            show: false,
            warning_text:''
        }
    }

    handleShow(){
        this.setState({ show: true });
    }

    handleClose(){
        this.setState({ show: false });
    }

    warningText(value){
        this.setState({ warning_text: value });
    }

    render(){
        return(
            <Modal
            show={this.state.show}
            onHide={this.handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <p>{this.state.warning_text}</p>
                    <button className="comman-theme-btn" onClick={this.handleClose}>Ok</button>
                </Modal.Body>
            </Modal>
        );
    }
}