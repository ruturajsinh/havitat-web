import React from 'react'
import { Modal } from 'react-bootstrap'

export default class SuccessModal extends React.Component{
    constructor(props){
        super(props);

        this.handleShow = this.handleShow.bind(this);  
        this.handleHide = this.handleHide.bind(this);
        this.redirectUrl = this.redirectUrl.bind(this);
        this.state = {
            show: false,
            success_message:'',
            redirect_url:''
        }
    }

    handleShow(){
        this.setState({ show: true });
    }

    handleHide(){
        this.setState({ show: false });
    }

    successText(value){
        this.setState({ success_message: value });
    }

    setRedirectUrl(value){
        this.setState({ redirect_url: value });
    }

    redirectUrl(){
        window.location.href = this.state.redirect_url;
    }

    render(){
        return(
            <Modal
            show={this.state.show}
            onHide={this.handleClose}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <p>{this.state.success_message}</p>
                    <button className="comman-theme-btn" onClick={this.handleHide, this.redirectUrl}>Ok</button>
                </Modal.Body>
            </Modal>
        )
    };
}