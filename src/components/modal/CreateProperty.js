import React, { Component } from 'react'
import { Col, Modal, Row } from 'react-bootstrap';

class CreateProperty extends Component {

    constructor(props){
        super(props);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.state = {
            show: false
        }
    }

    handleShow(){
        this.setState({ show: true });
    }

    handleClose(){
        this.setState({ show: false });
    }

    render() {
        return (
            <Modal
                show={this.state.show}
                onHide={this.handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton></Modal.Header>
                <Modal.Body>
                    <div className="add-property-header">
                        <img src={require('../../images/assets/logo_with_name.svg')} alt="logo name"></img>
                        <h6>Havitat</h6>
                    </div>
                    <div className="modal-orange-line"></div>
                    <h6 className="modal-head-line">Please provide your Property details</h6>
                    <div className="modal-container">
                        <div className="login-form comman-margin-top">
                            <div className="comman-input-area input-field">
                                <input id="property-address" type="text" name="property-address" />
                                <label htmlFor="property-address" >Enter Property Address</label>
                            </div>
                            <div className="comman-input-area input-field">
                                <label htmlFor="category" className="select-label">Enter Property Address</label>
                                <select id="category" className="command-dropdown pt-2">
                                    <option value="rent">Rent</option>
                                    <option value="sell">Sell</option>
                                </select>
                            </div>
                            <div className="comman-input-area input-field">
                                <label htmlFor="property-type" className="select-label">Enter Property Address</label>
                                <select id="property-type" className="command-dropdown pt-2">
                                    <option value="rent">Apartment</option>
                                    <option value="sell">Private</option>
                                </select>
                            </div>
                            <Row>
                                <Col md={6}>
                                    <a href="/add-property"> <button className="create-form-submit-small-btn" >Create Listing</button></a>
                                </Col>
                                <Col md={6}>
                                    <a href="/manage-my-property"><button className="create-form-submit-small-btn" >Manage My Property</button></a>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        )
    }
}

export default CreateProperty;