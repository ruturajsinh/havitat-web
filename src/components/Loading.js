import React from 'react'

export default class Loading extends React.Component{

    state = {
        visible: false
    }

    show(){
        this.setState({ visible: true });
    }

    hide(){
        this.setState({ visible: false });
    }

    render(){
        const { visible } = this.state;
        return(
            <>
                {
                    visible && <div className="loading">Loading&#8230;</div>
                }
            </>
        )
    }
}