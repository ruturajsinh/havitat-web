import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Storage extends Component{
    constructor(props){
        super(props);

        this.state = {
            storage_type:'',
            storage_area:'',
            storage_description:''
        };
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return (
            <div className="second-step-propety">
                <Row>
                    <Col md={3}>
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border">
                                <option>Storage Type</option>
                                <option value="warehouse">Warehouse</option>
                                <option value="storage_space">Storage Space</option>
                                <option value="locker">Locker</option>
                                <option value="garage">Garage</option>
                                <option value="parking_slot">Parking Slot</option>
                            </select>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="storage_area" type="tel" className="extra-input-height-42" name="storage_area" value={this.state.storage_area} onChange={ (e) => this.onChangeCheckNumeric(e) } required maxLength="25" />
                            <label htmlFor="storage_area">Storage Area</label>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="storage_description" type="tel" className="extra-input-height-42" name="storage_description" required maxLength="25" />
                            <label htmlFor="storage_description">Storage Description</label>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Storage;