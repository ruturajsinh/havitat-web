import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Commercial extends Component{
    constructor(props){
        super(props);

        this.state = {
            commercial_type: '',
            total_area:'',
            commercial_area:'',
            storage_area:'',
            office_area:'',
            type:''
        }
    }
    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return (
            <div className="second-step-propety">
            <Row>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.commercial_type} onChange={ (e) => { this.setState({ commercial_type: e.target.value }) } }>
                            <option>Commercial Type</option>
                            <option value="restaurant">Restaurant</option>
                            <option value="cafe">Cafe</option>
                            <option value="shop">Shop</option>
                            <option value="hotel">Hotel</option>
                            <option value="Hostel">Hostel</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="total_area" type="tel" name="total_area" className="extra-input-height-42" value={this.state.total_area} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                        <label htmlFor="total_area" >Total Area</label>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="commercial_area" type="tel" className="extra-input-height-42" name="commercial_area" value={this.state.commercial_area} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                        <label htmlFor="commercial_area" >Commercial Area</label>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="storage_area" type="tel" className="extra-input-height-42" name="storage_area" value={this.state.storage_area} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                        <label htmlFor="storage_area" >Storage Area</label>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="office_area" type="tel" className="extra-input-height-42" name="office_area" value={this.state.office_area} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                        <label htmlFor="office_area" >Office Area</label>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border">
                            <option>Type</option>
                            <option value="exisitng">Existing</option>
                            <option value="new">New</option>
                        </select>
                    </div>
                </Col>

            </Row>
            </div>
        );
    }
}

export default Commercial;