import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Leisure extends Component{
    constructor(props){
        super(props);

        this.state = {
            leisure_type: '',
            leisure_description:''
        };
    }

    render(){
        return (
            <div className="second-step-propety">
            <Row>
                <Col md={4}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.leisure_type} onChange={ (e) => { this.setState({ leisure_type: e.target.value }) } }>
                            <option>Leisure Type</option>
                            <option value="Holiday Property">Holiday Property</option>
                            <option value="Time Share">Time Share</option>
                            <option value="Hobby Farm">Hobby Farm</option>
                            <option value="Leisure Condo">Leisure Condo</option>
                            <option value="Holiday Villa">Holiday Villa</option>
                            <option value="Retirement Home">Retirement Home</option>
                        </select>
                    </div>
                </Col>
                <Col md={6}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="leisure_description" type="tel" className="extra-input-height-42" name="leisure_description" value={this.state.leisure_description} onChange={ (e) => { this.setState({ leisure_description: e.target.value }) } } required />
                        <label htmlFor="leisure_description" >Leisure Description</label>
                    </div>
                </Col>
            </Row>
            </div>
        );
    }
}

export default Leisure;