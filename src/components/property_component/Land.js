import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Land extends Component{
    constructor(props){
        super(props);

        this.state = {
            land_type:'',
            land_area:'',
            land_description:'',
            ownerships:'',
            land_designation:''
        };

    }

    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return (
            <div className="second-step-propety">
                <Row>
                    <Col md={3}>
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border" value={this.state.land_type} onChange={ (e) => { this.setState({ land_type: e.target.value }) } } >
                                <option>Land Type</option>
                                <option value="residental">Residential</option>
                                <option value="commercial">Commercial</option>
                                <option value="agricluture">Agricluture</option>
                                <option value="leisure">Leisure</option>
                                <option value="farm">Farm</option>
                                <option value="forest">Forest</option>
                                <option value="island">Island</option>
                            </select>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="land_area" type="tel" name="land_area" className="extra-input-height-42" value={this.state.land_area} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                            <label htmlFor="land_area">Land Area</label>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="land_description" type="text" className="extra-input-height-42" name="land_description" value={this.state.land_description} onChange={ (e) => { this.setState({ land_description: e.target.value }) } } required />
                            <label htmlFor="land_description" >Land Description</label>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border" value={this.state.ownerships} onChange={ (e) => { this.setState({ ownerships: e.target.value }) } }>
                                <option>Ownerships</option>
                                <option value="titles">Titles</option>
                                <option value="tax_dec">Tex Dec</option>
                                <option value="usage_right">Usage Right</option>
                            </select>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="land_designation" type="text" className="extra-input-height-42" name="land_designation" value={this.state.land_designation} onChange={ (e) => { this.setState({ land_designation: e.target.value }) } } required />
                            <label htmlFor="land_designation" >Land Designation</label>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Land;