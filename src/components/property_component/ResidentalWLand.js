import React, { Component } from 'react'
import {Row,Col} from 'react-bootstrap'

class ResidentalWLand extends Component{

    constructor(props){
        super(props);

        this.state = {
            lot_area:'',
            floor_area:'',
            rooms:'',
            bedrooms:'',
            bathrooms:'',
            kitchen:'',
            home_office:'',
            home_gym:'',
            home_cinema:'',
            home_bar:'',
            den:'',
            pool:'',
            pool_size:'',
            madis_room:'',
            number_of_maids_beds:'',
            driver_rooms:'',
            number_of_driver_beds:'',
            balcony:'',
            terrace:'',
            lanai:'',
            garden:'',
            garden_size:'',
            garage:'',
            parking:'',
            parking_slots:'',
            status:'',
            furnishing:'',
            association_dues:''
        }
    }

    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return(
            <div className="second-step-propety">
        <Row>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="lot-area" type="tel" name="lot_area" className="extra-input-height-42" value={this.state.lot_area} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="10" required />
                    <label htmlFor="lot-area" >Lot Area</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="floor_area" type="tel" className="extra-input-height-42" name="floor_area" value={this.state.floor_area} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="10" required />
                    <label htmlFor="floor_area" >Floor Area</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="rooms" type="tel" name="rooms" className="extra-input-height-42" value={this.state.rooms} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="2" required/>
                    <label htmlFor="rooms" >Rooms</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="bedrooms" type="tel" name="bedrooms" className="extra-input-height-42" value={this.state.bedrooms} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="2" required />
                    <label htmlFor="bedrooms" >Bedrooms</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="bathrooms" type="tel" name="bathrooms" className="extra-input-height-42" value={this.state.bathrooms} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="2" required/>
                    <label htmlFor="bathrooms" >Bathrooms</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={ this.state.kitchen } onChange={ (e) => { this.setState({ kitchen: e.target.value }) } }>
                        <option>Select Kitchen</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={ this.state.home_gym } onChange={ (e) => { this.setState({ home_gym: e.target.value }) } } >
                        <option>Home Gym</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.home_cinema} onChange={ (e) => { this.setState({ home_cinema: e.target.value }) } } >
                        <option>Home Cinema</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={ this.state.home_bar } onChange={ (e) => { this.setState({ home_bar: e.target.value }) } } >
                        <option>Home Bar</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>

            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={ this.state.den } onChange={ (e) => { this.setState({ den: e.target.value }) } } >
                        <option>Den</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.pool} onChange={ (e) => { this.setState({pool:e.target.value}) } } >
                        <option>Pool</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="pool_size" type="tel" className="extra-input-height-42" name="pool_size" value={this.state.pool_size} onChange={ (e) => this.onChangeCheckNumeric(e) } maxLength="5" required />
                    <label htmlFor="pool_size" >Pool Size</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border">
                        <option>Maids Rooms</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="number_of_maids_beds" type="tel" className="extra-input-height-42" name="number_of_maids_beds" value={this.state.number_of_maids_beds} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="5" required/>
                    <label htmlFor="number_of_maids_beds" >Number Of Mads Bed</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.driver_rooms} onChange={ (e) => { this.setState({ driver_rooms: e.target.value }) } } >
                        <option>Driver Rooms</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="number_of_driver_beds" type="tel" className="extra-input-height-42" name="number_of_driver_beds" value={this.state.number_of_driver_beds} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) }} maxLength="5" required/>
                    <label htmlFor="number_of_driver_beds" >Number Of Driver Bed</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={ this.state.balcony } onChange={ (e) => { this.setState({ balcony: e.target.value }) } } >
                        <option>Balcony</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.terrace} onChange={ (e) => { this.setState({ terrace: e.target.value }) } }>
                        <option>Terrace</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.lanai} onChange={ (e) => { this.setState({ lanai: e.target.value }) } } >
                        <option>Lanai</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.garden} onChange={ (e) => { this.setState({ garden: e.target.value }) } }>
                        <option>Garden</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="garden_size" type="tel" className="extra-input-height-42" name="garden_size" value={this.state.garden_size} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="5" required />
                    <label htmlFor="garden_size" >Garden Size</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.garage} onChange={ (e) => { this.setState({ garage: e.target.value }) } }>
                        <option>Garage</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.parking} onChange={ (e) => { this.setState({ parking: e.target.value }) } }>
                        <option>Parking</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area-with-border input-field">
                    <input id="parking_slots" type="tel" className="extra-input-height-42" name="parking_slots" value={this.state.parking_slots} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } maxLength="5" required/>
                    <label htmlFor="parking_slots" >Parking Slots</label>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.status} onChange={ (e) => { this.setState({ status: e.target.value }) } }>
                        <option>Status</option>
                        <option value="yes">Bare Shell</option>
                        <option value="no">Warm Shell</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.furnishing} onChange={ (e) => { this.setState({ furnishing: e.target.value }) } }>
                        <option>Furnishing</option>
                        <option value="basic">Basic</option>
                        <option value="fully">Fully</option>
                        <option value="luxury">Luxury</option>
                    </select>
                </div>
            </Col>
            <Col md={3}>
                <div className="comman-input-area input-field">
                    <select id="property-type" className="command-dropdown-with-border" value={this.state.association_dues} onChange={ (e) => { this.setState({ association_dues: e.target.value }) } }>
                        <option>Association Dues</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
            </Col>
        </Row>
    </div>
        );
    }

}

export default ResidentalWLand;