import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Advertising extends Component{
    constructor(props){
        super(props);

        this.state = {
            advertising_type:'',
            display_area:'',
            display_description:'',
            display_visibilty:'',
            eyes_per_day:''
        };
    }

    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return (
            <div className="second-step-propety">
            <Row>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.advertising_type} onChange={ (e) => { this.setState({ advertising_type: e.target.value }) } } >
                            <option>Advertising Type</option>
                            <option value="Billboard">Billboard</option>
                            <option value="Electronic Billboard">Electronic Billboard</option>
                            <option value="Billboard Space">Billboard Space</option>
                            <option value="Wall Signage">Wall Signage</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="display_area" type="tel" className="extra-input-height-42" name="display_area" value={this.state.display_area} onChange={ (e) => { this.hadleOnChange(e); this.onChangeCheckNumeric(e) } } required/>
                        <label htmlFor="display_area" >Display Area</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="display_description" type="text" className="extra-input-height-42" name="display_description" value={this.state.display_description} onChange={ (e) => { this.setState({ display_description: e.target.value }) } } required />
                        <label htmlFor="display_description" >Display Description</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="eyes_per_day" type="text" className="extra-input-height-42" name="eyes_per_day" value={this.state.eyes_per_day} onChange={ (e) => { this.setState({ eyes_per_day: e.target.value }) } } required />
                        <label htmlFor="eyes_per_day" >Eyes Per Day</label>
                    </div>
                </Col>
            </Row>
            </div>
        );
    }
}

export default Advertising;