import React,{Component} from 'react'
import {Row,Col} from 'react-bootstrap'

class ResidentalWOLand extends Component{

    constructor(props){
        super(props);

        this.state = {
            lot_area:'',
            floor_area:'',
            rooms:'',
            bedrooms:'',
            bathrooms:'',
            kitchen:'',
            home_office:'',
            home_gym:'',
            home_cinema:'',
            home_bar:'',
            den:'',
            pool:'',
            pool_size:'',
            madis_room:'',
            number_of_maids_beds:'',
            driver_rooms:'',
            number_of_driver_beds:'',
            balcony:'',
            terrace:'',
            lanai:'',
            garden:'',
            garden_size:'',
            parking:'',
            parking_slots:'',
            status:'',
            furnishing:'',
            association_dues:'',
            gym:'',
            play_room:'',
            function_room:'',
            reception:'',
            concirege:'',
            study_room:'',
            membership_club:'',
            commuunal_garden:'',
            commuunal_desk:'',
            retail_area:''
        }
    }

    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return(
            <div className="second-step-propety">
            <Row>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="floor_area" type="tel" className="extra-input-height-42" name="floor_area" value={this.state.floor_area} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } required maxLength="10" />
                        <label htmlFor="floor_area" >Floor Area</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="rooms" type="tel" className="extra-input-height-42" name="rooms" value={this.state.rooms} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } required maxLength="10" />
                        <label htmlFor="rooms" >Rooms</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="bedrooms" type="tel" className="extra-input-height-42" name="bedrooms" value={this.state.bedrooms} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) }} required maxLength="10" />
                        <label htmlFor="bedrooms" >Bedrooms</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="bathrooms" type="tel" className="extra-input-height-42" name="bathrooms" value={this.state.bathrooms} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } required maxLength="10" />
                        <label htmlFor="bathrooms" >Bathroom</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.kitchen} onChange={ (e) => { this.setState({ kitchen: e.target.value }) } }>
                            <option>Kitchen</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.home_office} onChange={ (e) => { this.setState({ home_office: e.target.value }) } }>
                            <option>Home Office</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.home_gym} onChange={ (e) => { this.setState({ home_gym: e.target.value }) } }>
                            <option>Home Gym</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.home_cinema} onChange={ (e) => { this.setState({ home_cinema: e.target.value }) } }>
                            <option>Home Cinema</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.home_bar} onChange={ (e) => { this.setState({ home_bar: e.target.value }) } }>
                            <option>Home Bar</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.den} onChange={ (e) => { this.setState({ den: e.target.value }) } }>
                            <option>Den</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.madis_room} onChange={ (e) => { this.setState({ madis_room: e.target.value }) } }>
                            <option>Maids Rooms</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="number_of_maids_beds" type="tel" className="extra-input-height-42" name="number_of_maids_beds" value={this.state.number_of_maids_beds} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } required maxLength="10" />
                        <label htmlFor="number_of_maids_beds" >Number Of Maids Rooms</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type"  className="command-dropdown-with-border" value={this.state.balcony} onChange={ (e) => this.setState({ balcony: e.target.value }) }>
                            <option>Balcony</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.terrace} onChange={ (e) => { this.setState({ terrace: e.target.value }) } }>
                            <option>Terrace</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.parking} onChange={ (e) => { this.setState({ parking: e.target.value }) } }>
                            <option>Parking</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="parking_slots" className="extra-input-height-42" type="tel" name="parking_slots" value={this.state.parking_slots} onChange={ (e) => { this.onChangeCheckNumeric(e); this.hadleOnChange(e) } } required maxLength="10" />
                        <label htmlFor="parking_slots" >Parking Slots</label>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.status} onChange={ (e) => { this.setState({ status: e.target.value }) } }>
                            <option>Status</option>
                            <option value="bare_shell">Bare Shell</option>
                            <option value="warm_shell">Warm Shell</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.furnishing} onChange={ (e) => { this.setState({ furnishing: e.target.value }) } }>
                            <option>Furnishing</option>
                            <option value="basic">Basic</option>
                            <option value="fully">Fully</option>
                            <option value="luxury">Luxury</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.association_dues} onChange={ (e) => { this.setState({ association_dues: e.target.value }) } }>
                            <option>Association Dues</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={12}>
                    <div className="left-side-title">
                        <div className="inner-title">
                            <h6>Amenities : </h6>
                        </div>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.pool} onChange={ (e) => { this.setState({ pool: e.target.value }) } }>
                            <option>Pool</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.gym} onChange={ (e) => { this.setState({ gym: e.target.value }) } }>
                            <option>Gym</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.play_room} onChange={ (e) => { this.setState({ play_room: e.target.value }) } }>
                            <option>Play Room</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.function_room} onChange={ (e) => { this.setState({ function_room: e.target.value }) } }>
                            <option>Function Room</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.reception} onChange={ (e) => { this.setState({ reception: e.target.value }) } }>
                            <option>Reception</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.concirege} onChange={ (e) => { this.setState({ concirege: e.target.value }) } }>
                            <option>Concierge Desk</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.study_room} onChange={ (e) => { this.setState({ study_room: e.target.value }) } }>
                            <option>Study Room</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.membership_club} onChange={ (e) => { this.setState({ membership_club: e.target.value }) } }>
                            <option>Membership Club</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.commuunal_garden} onChange={ (e) => { this.setState({ kitchen: e.target.commuunal_garden }) } }>
                            <option>Commuunal Garden</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.commuunal_desk} onChange={ (e) => { this.setState({ commuunal_desk: e.target.value }) } }>
                            <option>Commuunal Desk</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.retail_area} onChange={ (e) => { this.setState({ retail_area: e.target.value }) } }>
                            <option>Retail Area</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

            </Row>
            </div>
        );
    }

}

export default ResidentalWOLand;