import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Membership extends Component{
    constructor(props){
        super(props);

        this.state = {
            membership_type:'',
            membership_description:'',
            membership_right:'',
            membership_fees:''
        };
    }

    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return (
            <div className="second-step-propety">
            <Row>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.membership_type} value={ (e) => this.setState({ membership_type: e.target.value }) }>
                            <option>Membership Type</option>
                            <option value="Leisure Club">Leisure Club</option>
                            <option value="Golf Club">Golf Club</option>
                            <option value="Holiday Club">Holiday Club</option>
                            <option value="Sport Club">Sport Club</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="membership_description" type="text" className="extra-input-height-42" name="membership_description" value={this.state.membership_description} value={ (e) => { this.setState({ membership_description: e.target.value }) } } required/>
                        <label htmlFor="membership_description">Member Description</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="membership_rights" type="text" className="extra-input-height-42" name="membership_rights" value={this.state.membership_right} onChange={ (e) => { this.setState({ membership_right: e.target.value }) } } required/>
                        <label htmlFor="membership_rights">Membership Rights</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="membership_fees" type="text" className="extra-input-height-42" name="membership_fees" value={this.state.membership_fees} onChange={ (e) => { this.setState({ membership_fees: e.target.value }) } } required/>
                        <label htmlFor="membership_fees">Membership Fees</label>
                    </div>
                </Col>
            </Row>
            </div>
        );
    }
}

export default Membership;