import React, { Component } from 'react'
import { Row,Col } from 'react-bootstrap'

class Business extends Component{
    constructor(props){
        super(props);

        this.state = {
            business_type:'',
            business_description:'',
            turnover_per_annum:''
        }
    }

    render(){
        return (
            <div className="second-step-propety">
            <Row>
                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.business_type} onChange={ (e) => { this.setState({ business_type: e.target.value }) } }>
                            <option>Business Type</option>
                            <option value="Franchise">Franchise</option>
                            <option value="Shop">Shop</option>
                            <option value="Restaurant">Restaurant</option>
                            <option value="Cafe">Cafe</option>
                            <option value="Hotel/Hostel">Hotel/Hostel</option>
                            <option value="Food Cart">Food Cart</option>
                        </select>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="business_description" type="text" className="extra-input-height-42" name="business_description" value={this.state.business_description} onChange={ (e) => { this.setState({ business_description: e.target.value }) } } required />
                        <label htmlFor="business_description" >Business Description</label>
                    </div>
                </Col>
                <Col md={3}>
                    <div className="comman-input-area-with-border input-field">
                        <input id="turnover_per_annum" type="text" className="extra-input-height-42" name="turnover_per_annum" value={this.state.turnover_per_annum} onChange={ (e) => this.setState({ turnover_per_annum: e.target.value }) } required />
                        <label htmlFor="turnover_per_annum">Turnover Per Annum</label>
                    </div>
                </Col>
            </Row>
            </div>
        );
    }
}

export default Business;