import React, { Component } from 'react'
import {Row,Col} from 'react-bootstrap'

class Office extends Component{
    constructor(props){
        super(props);

        this.state = {
            floor_area:'',
            office_room:'',
            desks_seats:'',
            status:'',
            furnishing:'',
            association_dues:'',
            pool:'',
            gym:'',
            function_room:'',
            reception_room:'',
            study_room:'',
            communal_desk:'',
            retail_area:''
        };
    }

    hadleOnChange(e){
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onChangeCheckNumeric(e){
        const re = /^[0-9\b]+$/;
        if(e.target.value === '' || re.test(e.target.value)){
            this.setState({ [e.target.name] : e.target.value });
        }
    }

    render(){
        return(
            <div className="second-step-propety">
                <Row>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="floor_area" type="tel" className="extra-input-height-42" name="floor_area" value={this.state.floor_area} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                            <label htmlFor="floor_area" >Floor Area</label>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="office_room" type="tel" className="extra-input-height-42" name="office_room" value={this.state.office_room} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                            <label htmlFor="office_room" >Office Area</label>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area-with-border input-field">
                            <input id="desks_seats" type="tel" className="extra-input-height-42" name="office_room" value={this.state.desks_seats} onChange={ (e) => {this.onChangeCheckNumeric(e); this.hadleOnChange(e)} } required maxLength="10" />
                            <label htmlFor="desks_seats" >Desks/Seats</label>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border" value={this.state.status} onChange={ (e) => { this.setState({ status: e.target.value }) } } >
                                <option>Status</option>
                                <option value="bare_shell">Bare Shell</option>
                                <option value="warm_shell">Warm Shell</option>
                            </select>
                        </div>
                    </Col>
                    <Col md={3}>
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border" value={ this.state.furnishing } onChange={ (e) => { this.setState({ furnishing: e.target.value }) } } >
                                <option>Furnishing</option>
                                <option value="bare_shell">Basic</option>
                                <option value="warm_shell">Fully</option>
                                <option value="warm_shell">Luxury</option>
                            </select>
                        </div>
                    </Col>

                    <Col md={3}>
                        <div className="comman-input-area input-field">
                            <select id="property-type" className="command-dropdown-with-border" value={this.state.association_dues} onChange={ (e) => { this.setState({ association_dues: e.target.value }) } }>
                                <option>Association Dues</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </Col>

                    <Col md={12}>
                    <div className="left-side-title">
                        <div className="inner-title">
                            <h6>Amenities : </h6>
                        </div>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.pool} onChange={ (e) => { this.setState({ pool: e.target.value }) } } >
                            <option>Pool</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.gym} onChange={ (e) => { this.setState({ gym: e.target.value }) } } >
                            <option>Gym</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.function_room} onChange={ (e) => { this.setState({ function_room: e.target.value }) } } >
                            <option>Function Room</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.reception_room} onChange={ (e) => { this.setState({ reception_room: e.target.value }) } }>
                            <option>Reception Room</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={this.state.study_room} onChange={ (e) => { this.setState({ study_room: e.target.value }) } }>
                            <option>Study Room</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={ this.state.communal_desk } onChange={ (e) => { this.setState({ communal_desk: e.target.value }) } } >
                            <option>Communual Desk</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                <Col md={3}>
                    <div className="comman-input-area input-field">
                        <select id="property-type" className="command-dropdown-with-border" value={ this.state.retail_area } onChange={ (e) => {this.setState({ retail_area: e.target.value })} } >
                            <option>Retail Area</option>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </Col>

                </Row>
            </div>
        );
    }
}

export default Office;