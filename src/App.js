import React from 'react';
import './css/main.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-input-range/lib/css/index.css';
import 'reactjs-popup/dist/index.css'
import './css/style.scss'
import './css/responsive.css'
import 'react-confirm-alert/src/react-confirm-alert.css'
import Home from './pages/Home'

function App() {
  return <Home />;
}

export default App;
